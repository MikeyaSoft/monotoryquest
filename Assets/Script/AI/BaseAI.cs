﻿using UnityEngine;
using System.Collections;

public abstract class BaseAI:IAI {
		
		public virtual void DoneTileEvent(){
				//選択メニューをどう選択するか
				switch (GameManager.Instance.m_CurrentTile.KindOfTile) {
				case 0://表示なし
						break;
				case 1://物件
						BuyProperty();
						break;
				case 2://カードショップ
						BuyCard();
						break;
				case 3://銀行
						break;
				case 4://Mark
						break;
				case 5://Mark
						break;
				case 6://Mark
						break;
				case 7://Mark
						break;
				}
		}

		protected virtual void BuyCard(){
				//取り急ぎ、カードショップではカードは買わない処理を入れる
				ProductionManager.Instance.m_CurrentListWindow.HideAndTurnEnd();
		}

		protected virtual void BuyProperty(){
				if (GameManager.Instance.m_CurrentTile.HolderNumber == 0
						||
						GameManager.Instance.m_CurrentTile.HolderNumber == GameManager.Instance.m_CurrentPlayer.PlayerNumber) {
						//誰も所有していないもしくは自分が所有者の場合

						if (GameManager.Instance.m_CurrentPlayer.Money > GameManager.Instance.m_CurrentTile.PropertyPrice) {
								//物件価格よりも手持ちのお金が上
								ProductionManager.Instance.m_CurrentPopUp.onClickYesButton();
						} else {
								//物件を買えない場合
								//物件価格よりも手持ちのお金が上
								ProductionManager.Instance.m_CurrentPopUp.onClickNoButton();
						}

				} else {
				}
		}


}
