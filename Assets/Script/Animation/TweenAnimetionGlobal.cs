﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// グローバル座標を使用してのアニメーション
/// 主に対象を指定してその座標に移動するアニメ実装用
/// </summary>
public class TweenAnimetionGlobal : MonoBehaviour
{



		public enum easeType
		{
				easeInQuad,
				easeOutQuad,
				easeInOutQuad,
				easeInCubic,
				easeOutCubic,
				easeInOutCubic,
				easeInQuart,
				easeOutQuart,
				easeInOutQuart,
				easeInQuint,
				easeOutQuint,
				easeInOutQuint,
				easeInSine,
				easeOutSine,
				easeInOutSine,
				easeInExpo,
				easeOutExpo,
				easeInOutExpo,
				easeInCirc,
				easeOutCirc,
				easeInOutCirc,
				linear,
				spring,
				easeInBounce,
				easeOutBounce,
				easeInOutBounce,
				easeInBack,
				easeOutBack,
				easeInOutBack,
				easeInElastic,
				easeOutElastic,
				easeInOutElastic,
		}

		public easeType m_EaseType;

		public float animeTime = 1.0f;
		public bool playAnimationAutomaticaly = true;
		public float delayTime = 0;
		public Action CallBack;
		//public Action CallBackAnimation;
	
		/// <summary>
		/// 対象のグローバル座標
		/// </summary>
		public Vector3 targetPosition;
		public bool isCallBackAnimation=false;
		public string animeFxPath = "";
		/// <summary>
		/// プレイヤーのZ座標
		/// </summary>
		private const float PLAYER_Z_POSITION = -146.0f;

		void Start ()
		{
		}



		public void playAnimation (GameObject target)
		{


				targetPosition = target.transform.position;
				//targetPosition = new Vector3 (target.transform.position.x,target.transform.position.y,PLAYER_Z_POSITION);

				//Debug.Log (target.name);
				iTween.MoveTo (this.gameObject, iTween.Hash (
						"position", targetPosition,
						"time", animeTime, 
						"oncomplete", "_CallBack",//コールバックを定義
						"islocal", false,
						"oncompletetarget", this.gameObject,
						"delay", delayTime,
						"easeType", m_EaseType.ToString (),
						"space", Space.World
				));


		}

		private void _CallBack ()
		{
				
				if (CallBack != null) {
						Debug.Log (this.name+"_CallBack");
						CallBack ();
						/*
						if(isCallBackAnimation){
							Debug.Log (this.name+"isCallBackAnimation!");
							GameObject _ef = Instantiate(Resources.Load(animeFxPath)) as GameObject;
							_ef.transform.localScale = Vector3.one;
							_ef.transform.position = targetPosition;
						}
						*/

				} else {
				}

		}
				
}
