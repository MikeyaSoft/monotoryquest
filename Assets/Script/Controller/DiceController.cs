﻿using UnityEngine;
using System.Collections;

public class DiceController : MonoBehaviour {

		[SerializeField]
		private TweenRotation _TweenRotation;
		/// <summary>
		/// サイコロの目の数初期
		/// </summary>
		public int DiceNumber;

		void Awake(){

		}


	// Use this for initialization
	void Start () {
				AudioManager.Instance.PlaySE (4);
				Invoke ("StopAnime",0.6f);
	}

		public void StopAnime(){
				_TweenRotation.enabled = false;
				Debug.Log("DiceNumber="+DiceNumber);
				SetDiceSide (DiceNumber);
				Destroy (this.gameObject,1.0f);
		}
		/// <summary>
		/// ダイスの面をセット
		/// 1~6
		/// </summary>
		/// <param name="number">Number.</param>
		private void SetDiceSide(int number){
				this.transform.eulerAngles = GetDiceSide(number);
		}
		private Vector3 GetDiceSide(int side)
		{
				switch (side)
				{
				case 1: return new Vector3(180.0f, 0,0);
				case 2: return new Vector3(90.0f,0,0);
				case 3: return new Vector3(0, -90.0f, 0);
				case 4: return new Vector3(0, 90.0f, 0);
				case 5: return new Vector3(-90.0f, 0, 0);
				case 6: return new Vector3(0,0,0);
				}
				return Vector3.zero;
		}
	
}
