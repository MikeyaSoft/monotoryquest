﻿using UnityEngine;
using System.Collections;
using System;

public class ListItemController : MonoBehaviour {
		
		/// <summary>
		/// リストアイテムホバー時のコールバック
		/// </summary>
		public Action ListItemHoverCallBack;
		public UILabel NameLabel;
		// Use this for initialization
		void Start () {
				AudioManager.Instance.PlaySE (0);
		}
	
		public void onClickListItemButton(){
				WindowManager.Instance.onClickListItemButton ();
		}
		/// <summary>
		/// リストアイテムホバー時
		/// </summary>
		public void onHoverListItem(){
				GameManager.Instance.m_CurrentHoverListItemName = this.gameObject.name;
				if (ListItemHoverCallBack != null)
						ListItemHoverCallBack ();
		}
}
