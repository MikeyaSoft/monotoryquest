﻿using UnityEngine;
using System.Collections;

public class MarkFxController : MonoBehaviour {
		public float DeathTime=1.0f;
		[SerializeField]
		private UISprite m_MarkSprite;
		[SerializeField]
		private UILabel m_TargetLabel;
		[SerializeField]
		private UILabel m_MoneyLabel;


	// Use this for initialization
	void Start () {
				AudioManager.Instance.PlaySE (3);
				Destroy (this.gameObject,DeathTime);
	}
		/// <summary>
		/// 通行量支払いの演出の情報をセットする
		/// </summary>
		public void SetTaxFx(string playerName,string targetName,int money){
				m_TargetLabel.text = targetName+" → "+playerName;
				m_MoneyLabel.text = money + "G";
		}
		/// <summary>
		/// 銀行獲得の演出の情報をセットする
		/// </summary>
		public void SetBankFx(int money){
				m_TargetLabel.gameObject.SetActive(false);
				m_MoneyLabel.text = money + "G"+"獲得！";
		}

		/// <summary>
		/// Markの模様をセット
		/// </summary>
		/// <param name="_Mark">Mark.</param>
		public void SetMarkSprite(int KindOfTile){
				StaticData.MarkName m_Mark =StaticData.MarkName.Tile_Crow;
				switch(KindOfTile){
						case 4:
						m_Mark =StaticData.MarkName.Tile_Hart;
						break;
						case 5:
						m_Mark =StaticData.MarkName.Tile_Crow;
						break;
						case 6:
						m_Mark =StaticData.MarkName.Tile_Spade;
						break;
						case 7:
						m_Mark =StaticData.MarkName.Tile_Dia;
						break;
				}

				m_MarkSprite.spriteName = m_Mark.ToString();
		}
}
