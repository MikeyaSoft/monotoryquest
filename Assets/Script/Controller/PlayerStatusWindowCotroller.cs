﻿using UnityEngine;
using System.Collections;

public class PlayerStatusWindowCotroller : MonoBehaviour {
		public UILabel NameLabel;
		public UILabel MoneyLabel;
		public UISprite CrowSprite;
		public UISprite DiaSprite;
		public UISprite HartSprite;
		public UISprite SpadeSprite;
		public Player m_Player;

	// Use this for initialization
	void Start () {
				initMarkSpriteColor ();
				NameLabel.text = m_Player.UserName;
				MoneyLabel.text = "所持金：" + m_Player.Money + "G";
	}
		public void initMarkSpriteColor(){
				SetDisableColor (CrowSprite);
				SetDisableColor (DiaSprite);
				SetDisableColor (HartSprite);
				SetDisableColor (SpadeSprite);
		}
		/// <summary>
		/// プレイヤーの状態と表示を同期する
		/// </summary>
		public void SyncPlayerStatu(){
				MoneyLabel.text = "所持金：" + m_Player.Money + "G";
				for(int i=0;m_Player.MarkList.Count>i;i++){
						switch(m_Player.MarkList[i]){
						case 4:
								SetEnableColor (HartSprite);
								break;
						case 5:
								SetEnableColor (CrowSprite);
								break;
						case 6:
								SetEnableColor (SpadeSprite);
								break;
						case 7:
								SetEnableColor (DiaSprite);
								break;
						}
				}
		}

		private void SetDisableColor(UISprite _target){
				_target.color = new Color (0.5f, 0.5f, 0.5f, 1.0f);
		}
		public void SetEnableColor(UISprite _target){
				_target.color = new Color (1.0f, 1.0f, 1.0f, 1.0f);
		}
}
