﻿using UnityEngine;
using System.Collections;
using System;

public class PopUpController : MonoBehaviour {
	
		[SerializeField]
		private UIButton m_YesButton;
		[SerializeField]
		private UIButton m_NoButton;
		/// <summary>
		/// 表示テキスト
		/// </summary>
		public UILabel m_PopUpLabel;
		public Action YesCallBack;
		public Action NoCallBack;
		/// <summary>
		/// ポップアップか否か、
		/// 違う場合は選択メニューになる
		/// </summary>
		public bool isPopUp = true;
		/// <summary>
		/// 自動消滅時間
		/// </summary>
		public float DeathTime = 2.0f;
		/// <summary>
		/// 表示パネル
		/// </summary>
		[SerializeField]
		private UIPanel m_Panel;
		/// <summary>
		/// windowを閉じるときのアニメ
		/// </summary>
		[SerializeField]
		private TweenScale _HideAnime;


	// Use this for initialization
	void Start () {
				AudioManager.Instance.PlaySE (0);
				if (isPopUp) {//ポップアップの場合
						m_YesButton.gameObject.SetActive(false);
						m_NoButton.gameObject.SetActive(false);
						Destroy(this.gameObject,DeathTime);
				} else {//ポップアップでない場合はボタンを表示する
				}
	}
		/// <summary>
		/// 表示パネルのdepthを１あげる
		/// </summary>
		public void AddUIPanelDepth(int _depth = -1){
				if (_depth == -1)
						_depth = this.m_Panel.depth;
				m_Panel.depth = _depth + 1;
		}


		public void onClickYesButton(){
				WindowManager.Instance.onClickPopUpYesButton ();

		}


		public void onClickNoButton(){
				WindowManager.Instance.onClickPopUpNoButton ();

		}

		public void HideMenu(){
				AudioManager.Instance.PlaySE (1);
					Destroy(this.gameObject);
		}
		/// <summary>
		/// メニューを閉じると同時にターンを次の人に回す
		/// </summary>
		public void HideAndTurnEnd(){
				AudioManager.Instance.PlaySE (1);
				_HideAnime.enabled = true;
				//取り急ぎターン進行処理を入れる
				GameManager.Instance.TurnCycleRPC ();
				Destroy(this.gameObject,0.4f);
		}
	
}
