﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class ScrollListWindowController : MonoBehaviour {

		[SerializeField]
		public GameObject m_ListItem;
		[SerializeField]
		public UIButton m_NoButton;
		/// <summary>
		/// アイテム格納グリッド
		/// </summary>
		public UIGrid m_UIGrid;

		/// <summary>
		/// 表示テキスト
		/// </summary>
		public UILabel m_PopUpLabel;
		/// <summary>
		/// リストアイテムクリック時のコールバック
		/// </summary>
		public Action ListItemCallBack;
		/// <summary>
		/// リストアイテムの生成
		/// </summary>
		public Action AppendListItems;

		/// <summary>
		/// 自動消滅時間
		/// </summary>
		public float DeathTime = 2.0f;
		/// <summary>
		/// 表示パネル
		/// </summary>
		[SerializeField]
		public UIPanel m_Panel;
		/// <summary>
		/// windowを閉じるときのアニメ
		/// </summary>
		[SerializeField]
		private TweenScale _HideAnime;
		/// <summary>
		/// サイドのサブウィンドウ
		/// </summary>
		[SerializeField]
		private GameObject m_SubWindow;
		/// <summary>
		/// サブウィンドウの吹き出し文言
		/// </summary>
		[SerializeField]
		private UILabel m_FukidasiLabel;
		/// <summary>
		/// リストアイテムを格納する
		/// </summary>
		public List<ListItemController> m_ItemList;


		// Use this for initialization
		void Start () {
				m_ItemList = new List<ListItemController> ();
				AudioManager.Instance.PlaySE (0);
				if (AppendListItems != null) {
						AppendListItems ();
				}
				
		}
		/// <summary>
		/// サブウィンドウの文言をセットする
		/// </summary>
		/// <param name="text">Text.</param>
		public void SetSubWindowText(string text){
				m_FukidasiLabel.gameObject.SetActive (false);
				m_FukidasiLabel.gameObject.SetActive (true);
				m_FukidasiLabel.text = text;
		}

		/// <summary>
		/// 表示パネルのdepthを１あげる
		/// </summary>
		public void AddUIPanelDepth(){
				m_Panel.depth = m_Panel.depth + 1;
		}

		public void onClickNoButton(){
				WindowManager.Instance.onClickListWindowNoButton ();
		}

		public void HideMenu(){
				Debug.Log ("HideMenu");
				AudioManager.Instance.PlaySE (1);
				_HideAnime.enabled = true;
				Destroy(this.gameObject,0.4f);
		}
		/// <summary>
		/// メニューを閉じると同時にターンを次の人に回す
		/// </summary>
		public void HideAndTurnEnd(){
				AudioManager.Instance.PlaySE (1);
				_HideAnime.enabled = true;
				//取り急ぎターン進行処理を入れる
				GameManager.Instance.TurnCycleRPC ();
				Destroy(this.gameObject,0.4f);
		}

		/// <summary>
		/// カードショップ時のカードを売る処理
		/// </summary>
		public void SaleCardItem(){
				EventManager.Instance.SaleCardListItem ();
		}
		/// <summary>
		/// カードショップ時のカードを買う処理
		/// </summary>
		public void BuyCardItem(){
				EventManager.Instance.BuyCardListItem ();
		}
}
