﻿using UnityEngine;
using System.Collections;

public class TextFxController : MonoBehaviour {
		
		/// <summary>
		/// 対象のテキストラベル
		/// </summary>
		public UILabel m_Label;

		/// <summary>
		/// 一定時間後に消滅
		/// </summary>
		public float DeathTime = 2.0f;
	
	// Use this for initialization
	void Start () {
				Invoke ("CentringScroll",DeathTime-0.1f);
				Destroy (this.gameObject,DeathTime);

	}
		/// <summary>
		/// 表示をセンタリング
		/// </summary>
		private void CentringScroll(){
				GameManager.Instance.CenteringScroll ();
		}
}
