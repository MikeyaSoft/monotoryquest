﻿using UnityEngine;
using System.Collections;
/// <summary>
/// スクロールビューのアップデート
/// </summary>
public class UpDateScrollView : MonoBehaviour {
		[SerializeField]
		private UIPanel m_UIPanel;
		private int count;
		[SerializeField]
		private bool SyncMapPosFromScroll;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
				count++;
				if (count > 1) {
						m_UIPanel.alpha = 0.9f;
						m_UIPanel.alpha = 1.0f;
						count = 0;
						if(SyncMapPosFromScroll)
						GameManager.Instance.SyncMapPosFromScroll ();
				}
	}
}
