﻿using UnityEngine;
using System.Collections;

public class WindowController : MonoBehaviour {

		[SerializeField]
		private GameObject m_Shild;
		public UILabel InputLabel;
		private bool ToggleMenu;

		[SerializeField]
		private UILabel MoveCountNum;

	// Use this for initialization
	void Start () {
				ToggleMenu = false;
	
	}

		/// <summary>
		/// 更新
		/// </summary>
		public void SetMoveCountLabel(string _text){
				MoveCountNum.text = _text;
		}

		public void ShowShild(){
				m_Shild.SetActive (true);
		}

		public void HideShild(){
				m_Shild.SetActive (false);
		}
		/// <summary>
		/// Windowのトグル表示
		/// </summary>
		public void ToggleWindow(){
				if(ToggleMenu){
						ShowWindow ();
						ToggleMenu = false;
				}else{
						HideWindow ();
						ToggleMenu = true;
				}
		}
	
		public void HideWindow(){
				this.gameObject.SetActive (false);
		}
		public void ShowWindow(){
				this.gameObject.SetActive (true);
		}
	
}
