﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIManager : SingletonMonoBehaviour<AIManager>
{
	
		/// <summary>
		/// 現在プレイヤーがコンピューターの場合に実行される
		/// </summary>
		public IEnumerator AI ()
		{
				Debug.Log ("AI");
				yield return new WaitForSeconds(1.0f);
				Debug.Log ("AI2");
				if (GameManager.Instance.m_CurrentPlayer.DestinationList == null || GameManager.Instance.m_CurrentPlayer.DestinationList.Count == 0) {
						Debug.Log ("CalcDestinationList!!");
						//現在プレイヤーの目的地リストがnull,もしくは何も入ってない場合は目的地までの経路を取得する
						CalcDestinationList ();
				}
				yield return new WaitForSeconds(1.0f);
				Debug.Log ("AI3");
				MoveCurrentPlayer ();
				yield return new WaitForSeconds(1.0f);
				//タイルを選択しそのマスに移動
				GameManager.Instance.m_CurrentTile.onClickTile ();
				yield return new WaitForSeconds(1.0f);
				Debug.Log ("AI4");

		}
		/// <summary>
		/// TileEventの実行
		/// Delegateで実行される
		/// </summary>
		public void TileEventAI(){
				GameManager.Instance.m_CurrentPlayer.ai.DoneTileEvent();
				/*
				//選択メニューをどう選択するか
				switch (GameManager.Instance.m_CurrentTile.KindOfTile) {
				case 0://表示なし
						break;
				case 1://物件
						if (GameManager.Instance.m_CurrentTile.HolderNumber == 0
						    ||
						    GameManager.Instance.m_CurrentTile.HolderNumber == GameManager.Instance.m_CurrentPlayer.PlayerNumber) {
								//誰も所有していないもしくは自分が所有者の場合

								if (GameManager.Instance.m_CurrentPlayer.Money > GameManager.Instance.m_CurrentTile.PropertyPrice) {
										//物件価格よりも手持ちのお金が上
										ProductionManager.Instance.m_CurrentPopUp.onClickYesButton();
								} else {
										//物件を買えない場合
										//物件価格よりも手持ちのお金が上
										ProductionManager.Instance.m_CurrentPopUp.onClickNoButton();
								}

						} else {
						}
						break;
				case 2://カードショップ
						//取り急ぎ、カードショップではカードは買わない処理を入れる
						ProductionManager.Instance.m_CurrentListWindow.HideAndTurnEnd();
						break;
				case 3://銀行
						break;
				case 4://Mark
						break;
				case 5://Mark
						break;
				case 6://Mark
						break;
				case 7://Mark
						break;
				}
				*/
		}

		/// <summary>
		/// 経路にそって移動する処理
		/// </summary>
		private void MoveCurrentPlayer ()
		{
				//現在プレイヤーがサイコロを振る処理
				GameManager.Instance.SetDiceNumberFromCurrentPlayer ();
				//移動経路の選択
				//ステータスが０のタイルをforで回す
				for (int i = 0; GameManager.Instance.m_CurrentPlayer.RemovableAreaTileList.Count > i; i++) {
						if (GameManager.Instance.m_CurrentPlayer.RemovableAreaTileList [i].MigrationPathwayTileList [0]
						    ==
						    GameManager.Instance.m_CurrentPlayer.DestinationList [0]) {//移動経路が一致する場合
								//移動分のタイルを経路リストから引く
								for (int j = 0; GameManager.Instance.m_CurrentPlayer.RemovableAreaTileList [i].MigrationPathwayTileList.Count > j; j++) {
										//要素がある場合は引く
										if (GameManager.Instance.m_CurrentPlayer.DestinationList.Contains (GameManager.Instance.m_CurrentPlayer.RemovableAreaTileList [i].MigrationPathwayTileList [j])) {
												GameManager.Instance.m_CurrentPlayer.DestinationList.Remove (GameManager.Instance.m_CurrentPlayer.RemovableAreaTileList [i].MigrationPathwayTileList [j]);	
										}


								}
								GameManager.Instance.m_CurrentTile = GameManager.Instance.m_CurrentPlayer.RemovableAreaTileList [i];
								return;
						}
				}


				/*------------ ------------*/
				//目的地がnullの場合は再度、経路計算をして経路にそって移動する処理を行う
				/*------------ ------------*/
				Debug.LogError ("MoveCurrentPlayer=null");
				//歩数を一旦キャッシュ
				int PrevMoveCount = GameManager.Instance.m_CurrentPlayer.MoveCount;
				//タイルの状態を初期化
				TileManager.Instance.initTiles ();
				//目的地取得処理
				CalcDestinationList ();
				GameManager.Instance.m_CurrentPlayer.MoveCount = PrevMoveCount;
				//経路計算
				GameManager.Instance.CalculationCurrentPlayer ();
				//移動経路の選択
				//ステータスが０のタイルをforで回す
				for (int i = 0; GameManager.Instance.m_CurrentPlayer.RemovableAreaTileList.Count > i; i++) {
						if (GameManager.Instance.m_CurrentPlayer.RemovableAreaTileList [i].MigrationPathwayTileList [0]
								==
								GameManager.Instance.m_CurrentPlayer.DestinationList [0]) {//移動経路が一致する場合
								//移動分のタイルを経路リストから引く
								for (int j = 0; GameManager.Instance.m_CurrentPlayer.RemovableAreaTileList [i].MigrationPathwayTileList.Count > j; j++) {
										//要素がある場合は引く
										if (GameManager.Instance.m_CurrentPlayer.DestinationList.Contains (GameManager.Instance.m_CurrentPlayer.RemovableAreaTileList [i].MigrationPathwayTileList [j])) {
												GameManager.Instance.m_CurrentPlayer.DestinationList.Remove (GameManager.Instance.m_CurrentPlayer.RemovableAreaTileList [i].MigrationPathwayTileList [j]);	
										}


								}
								GameManager.Instance.m_CurrentTile = GameManager.Instance.m_CurrentPlayer.RemovableAreaTileList [i];
								return;
						}
				}

		}

		/// <summary>
		/// 現在プレイヤーの目的地を計算、取得する
		/// </summary>
		public void CalcDestinationList ()
		{
				for (int i = 1; TileManager.Instance.TileList.Count > i; i++) {
						//歩数をセット
						GameManager.Instance.m_CurrentPlayer.MoveCount = i;
						//経路計算
						GameManager.Instance.CalculationCurrentPlayer ();

						//ステータスが０のタイルをforで回す
						for (int j = 0; GameManager.Instance.m_CurrentPlayer.RemovableAreaTileList.Count > j; j++) {
								//ステータスが0のタイル中の経路情報を回す
								for (int k = 0; GameManager.Instance.m_CurrentPlayer.RemovableAreaTileList [j].MigrationPathwayTileList.Count > k; k++) {
										if (GameManager.Instance.m_CurrentPlayer.MarkList.Count >= 4) {//マークをすべて集め終わっている場合
												if (GameManager.Instance.m_CurrentPlayer.RemovableAreaTileList [j].MigrationPathwayTileList [k].KindOfTile == 3) {//リスト中に銀行マスが含まれている
														//目的地リストを複製して作成
														GameManager.Instance.m_CurrentPlayer.DestinationList = new List<TileBase> (GameManager.Instance.m_CurrentPlayer.RemovableAreaTileList [j].MigrationPathwayTileList);	
														//タイルの状態を初期化
														TileManager.Instance.initTiles ();
														return;
												}

										} else {//マークがまだ全部集まってない
												//長いのでキャッシュ
												int _TileKind = GameManager.Instance.m_CurrentPlayer.RemovableAreaTileList [j].MigrationPathwayTileList [k].KindOfTile;
												if (_TileKind >= 4 && _TileKind <= 7) {//タイルの種類がマークの時
														if (!GameManager.Instance.m_CurrentPlayer.MarkList.Contains (_TileKind)) {
																//目的地リストを複製して作成
																GameManager.Instance.m_CurrentPlayer.DestinationList = new List<TileBase> (GameManager.Instance.m_CurrentPlayer.RemovableAreaTileList [j].MigrationPathwayTileList);	
																//タイルの状態を初期化
																TileManager.Instance.initTiles ();
																return;
														}
												}
												
										}
								}
						}
						//タイルの状態を初期化
						TileManager.Instance.initTiles ();

				}

		}
	
}
