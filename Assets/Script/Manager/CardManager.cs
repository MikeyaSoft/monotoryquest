﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// カード使用時の効果を定義
/// </summary>
public class CardManager : SingletonMonoBehaviour<CardManager>
{

		// Use this for initialization
		void Start ()
		{
	
		}

		/// <summary>
		/// idから使用スキルを実行する
		/// 0	1進むカード
		///1	2進むカード
		///2	3進むカード
		///3	4進むカード
		///4	5進むカード
		///5	6進むカード
		/// </summary>
		/// <param name="id">Identifier.</param>
		public void UseCard (int id)
		{
				AudioManager.Instance.PlaySE (3);
				switch (id) {
				case 0:
						SetDiceNumberFromCurrentPlayer (1);
						break;
				case 1:
						SetDiceNumberFromCurrentPlayer (2);
						break;
				case 2:
						SetDiceNumberFromCurrentPlayer (3);
						break;
				case 3:
						SetDiceNumberFromCurrentPlayer (4);
						break;
				case 4:
						SetDiceNumberFromCurrentPlayer (5);
						break;
				case 5:
						SetDiceNumberFromCurrentPlayer (6);
						break;
				}
		}


		/// <summary>
		/// 引数の値だけサイコロの目が出る
		/// </summary>
		public void SetDiceNumberFromCurrentPlayer(int Number){
				GameManager.Instance.m_CurrentPlayer.MoveCount = Number;
				GameManager.Instance.CalculationCurrentPlayer ();
				ProductionManager.Instance.AppendDice (Number);


		}

		/// <summary>
		/// カードリストからidを用いて検索しカードデータを返す
		/// </summary>
		/// <returns>The card data from identifier.</returns>
		/// <param name="_CardList">Card list.</param>
		public CardBase GetCardDataFromId(List<CardBase> _CardList,int _id){
				for(int i=0;_CardList.Count>i;i++){
						if (_CardList [i].CardId == _id)
								return _CardList [i];
				}
				return null;
		}

}
