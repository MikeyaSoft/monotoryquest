﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MiniJSON;

public class DataManager : SingletonMonoBehaviour<DataManager>
{

		public List<string[]> MapData;
		public List<CardBase> CardData;

		private void TestDump ()
		{
				for (int i = 0; MapData.Count > i; i++) {
						for(int j=0;MapData [i].Length>j;j++){
								Debug.Log (MapData [i] [j]);
						}
				}

		}

	
		void Awake ()
		{
				//map
				List<Dictionary<string, string>> j_MapData;
				j_MapData = JsonToIDictionaryList ("json/map");
				MapData = DictConverter (j_MapData);

				//card
				CardData = new List<CardBase> ();
				List<Dictionary<string, string>> j_CardData;
				j_CardData = JsonToIDictionaryList ("json/Card");
				List<string[]> s_CardData = DictConverter (j_CardData);
				for(int i=0;s_CardData.Count>i;i++){
						
						CardBase _card = new CardBase (
								int.Parse(s_CardData[i][0]),
								s_CardData[i][1],
								s_CardData[i][2],
								int.Parse(s_CardData[i][3]),
								int.Parse(s_CardData[i][4]),
								int.Parse(s_CardData[i][5])
						);
						CardData.Add (_card);
				}


				//testDumpCardData ();

		}

		public void testDumpCardData(){
				for(int i=0;CardData.Count>i;i++){
						Debug.Log (CardData[i].CardName+":"+CardData[i].CardId);
				}
		}


		/*--dataConverter--*/
		/// <summary>
		/// List<Dictionary<string, string>> を　List<string[]>に変換する
		/// </summary>
		/// <returns>The converter.</returns>
		private List<string[]> DictConverter (List<Dictionary<string, string>> TargetDictList)
		{
				List<string[]> _ListString = new List<string[]> ();

				for (int i = 0; TargetDictList.Count > i; i++) {
						string[] _DataArry = new string[TargetDictList [i].Count];
						// キーの列挙
						int count = 0;
						foreach (string key in TargetDictList [i].Keys) {
								//Console.WriteLine("{0} : {1}", key, );
								_DataArry [count] = TargetDictList [i] [key];
								count++;
						}
						_ListString.Add (_DataArry);
				}

				return _ListString;

		}



		/*---Jsonperther--*/

		/// <summary>JSONパース List<IDictionary>を生成 </summary>
		public List<Dictionary<string, string>> JsonToIDictionaryList (string FilePass)
		{

				TextAsset jsonAsset = Resources.Load (FilePass)as TextAsset;
				string json = jsonAsset.text;
				IList m_Ilist = (IList)Json.Deserialize (json);
				List<IDictionary> IDictionaryList = new List<IDictionary> ();

				for (int i = 0; m_Ilist.Count > i; i++) {
						IDictionaryList.Add ((IDictionary)m_Ilist [i]);
				}

				List<Dictionary<string, string>> m_st_dict = IDictionaryToStringDictionaryList (IDictionaryList);

				return m_st_dict;

		}

		/// <summary>IDictionaryをstring型のdictionaryに変換</summary>
		public List<Dictionary<string, string>> IDictionaryToStringDictionaryList (List<IDictionary> IDictionaryList)
		{
				List<Dictionary<string, string>> m_list = new List<Dictionary<string, string>> ();

				for (int i = 0; IDictionaryList.Count > i; i++) {
						Dictionary<string, string> _dict = new Dictionary<string, string> ();
						foreach (string key in IDictionaryList[0].Keys) {
								string val = "" + IDictionaryList [i] [key];
								_dict.Add (key, val);
						}
						m_list.Add (_dict);
				}

				return m_list;

		}

		/*---csv perther----*/

		private string LoadData (string path)
		{
				// TextAssetとして、Resourcesフォルダからテキストデータをロードする
				TextAsset stageTextAsset = Resources.Load (path) as TextAsset;
				// 文字列を代入
				string _Data = stageTextAsset.text;
				return _Data;

		}

		private List<string[]> PerthCSV (string _Data)
		{

				string[] _DataArry = _Data.Split ('\n');
				List<string[]> _DataList = new List<string[]> ();
				for (int i = 1; _DataArry.Length > i; i++) {//i=1にして最初の行は入れない
						_DataList.Add (_DataArry [i].Split (','));
				}
				return _DataList;

		}



	
}
