﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// ゲーム中でマスに止まった時のイベントを定義
/// </summary>
public class EventManager : SingletonMonoBehaviour<EventManager> {

		public List<CardBase> ShopCardList;
		/// <summary>
		/// The m photon view.
		/// </summary>
		[SerializeField]
		private PhotonView m_PhotonView;

	// Use this for initialization
	void Start () {
				//ショップで販売するカードの初期化
				SetShopCardList ();
	}
		/// <summary>
		/// マスの種類によってイベントを出し分ける
		/// 1:物件
		/// 2:カードショップ
		/// 3:銀行
		/// 4:ハート
		/// 5:クローバー
		/// 6:スペード
		/// 7:ダイア
		/// </summary>
		/// <param name="id">Identifier.</param>
		public void ExcuteEvent (int id)
		{
				switch (id) {
				case 0://表示なし
						//取り急ぎターン進行処理を入れる
						GameManager.Instance.TurnCycleRPC ();
						break;
				case 1://物件

						if (GameManager.Instance.m_CurrentTile.HolderNumber == 0) {//誰も所有していない場合
								ProductionManager.Instance.AppendPopUp (false,
										"現在所持金:" + GameManager.Instance.m_CurrentPlayer.Money + "G" + "\n" +
										"物件名:" + GameManager.Instance.m_CurrentTile.PropertyName + "\n" +
										"物件価格:" + GameManager.Instance.m_CurrentTile.PropertyPrice + "\n" +
										"\n" +
										"この物件を購入しますか？"
								);
								//いいえボタン押下時のコールバック
								ProductionManager.Instance.m_CurrentPopUp.NoCallBack = ClosePopUp;
								//はいボタン押下時のコールバック
								ProductionManager.Instance.m_CurrentPopUp.YesCallBack = BuyProperty;
						} else if (GameManager.Instance.m_CurrentTile.HolderNumber == GameManager.Instance.m_CurrentPlayer.PlayerNumber) {//所有者が自分の場合

								if (GameManager.Instance.m_CurrentTile.PropertyRanck < 2) {
										ProductionManager.Instance.AppendPopUp (false,
												"現在所持金:" + GameManager.Instance.m_CurrentPlayer.Money + "G" + "\n" +
												"物件名:" + GameManager.Instance.m_CurrentTile.PropertyName + "\n" +
												"物件価格:" + GameManager.Instance.m_CurrentTile.PropertyPrice+ "G" + "\n" +
												"\n" +
												"この物件を増資しますか？"
										);
										//いいえボタン押下時のコールバック
										ProductionManager.Instance.m_CurrentPopUp.NoCallBack = ClosePopUp;
										//はいボタン押下時のコールバック
										ProductionManager.Instance.m_CurrentPopUp.YesCallBack = CapitalIncrease;
								} else {
										ProductionManager.Instance.AppendPopUp(true,"これ以上増資できません");
										//取り急ぎターン進行処理を入れる
										GameManager.Instance.TurnCycleRPC ();
								}


								
						} else {//通行量を払う処理
								GameManager.Instance.m_CurrentPlayer.Money -= GameManager.Instance.m_CurrentTile.PropertyTax;
								GameManager.Instance.GetPlayerFromPlayerNumber(GameManager.Instance.m_CurrentTile.HolderNumber).Money += GameManager.Instance.m_CurrentTile.PropertyTax;

								//演出
								ProductionManager.Instance.AppendTaxFx(
										GameManager.Instance.m_CurrentTile.gameObject,
										GameManager.Instance.GetPlayerFromPlayerNumber(GameManager.Instance.m_CurrentTile.HolderNumber).UserName,
										GameManager.Instance.m_CurrentPlayer.UserName,
										GameManager.Instance.m_CurrentTile.PropertyTax
								);
								//取り急ぎターン進行処理を入れる
								GameManager.Instance.TurnCycleRPC ();

						}




						break;
				case 2://カードショップ
						ProductionManager.Instance.AppendCardShopWindow();
						break;
				case 3://銀行
						BankEvent(GameManager.Instance.m_CurrentPlayer,1.2f);
						//取り急ぎターン進行処理を入れる

								GameManager.Instance.TurnCycleRPC ();
						break;
				case 4:
						if (GameManager.Instance.m_CurrentPlayer.m_CardList.Count >= StaticData.MAX_CARD_NUM) {//カードをこれ以上持てない場合
								ProductionManager.Instance.AppendPopUp(true,"これ以上カードを持てません");
						} else {
								GetRandomCard ();
						}

						//取り急ぎターン進行処理を入れる
						GameManager.Instance.TurnCycleRPC ();
						break;
				case 5:
						if (GameManager.Instance.m_CurrentPlayer.m_CardList.Count >= StaticData.MAX_CARD_NUM) {//カードをこれ以上持てない場合
								ProductionManager.Instance.AppendPopUp(true,"これ以上カードを持てません");
						} else {
								GetRandomCard ();
						}
						//取り急ぎターン進行処理を入れる
						GameManager.Instance.TurnCycleRPC ();
						break;
				case 6:
						if (GameManager.Instance.m_CurrentPlayer.m_CardList.Count >= StaticData.MAX_CARD_NUM) {//カードをこれ以上持てない場合
								ProductionManager.Instance.AppendPopUp(true,"これ以上カードを持てません");
						} else {
								GetRandomCard ();
						}
						//取り急ぎターン進行処理を入れる
						GameManager.Instance.TurnCycleRPC ();
						break;
				case 7:
						if (GameManager.Instance.m_CurrentPlayer.m_CardList.Count >= StaticData.MAX_CARD_NUM) {//カードをこれ以上持てない場合
								ProductionManager.Instance.AppendPopUp(true,"これ以上カードを持てません");
						} else {
								GetRandomCard ();
						}
						//取り急ぎターン進行処理を入れる
						GameManager.Instance.TurnCycleRPC ();
						break;
				}
		}

		/// <summary>
		/// ショップで販売するためのカードをセットする
		/// </summary>
		public void SetShopCardList(int one=0,int two=1,int tree=2,int foure=3,int five=4){
				ShopCardList = new List<CardBase> ();
				CardBase _card;
				_card = CardManager.Instance.GetCardDataFromId (DataManager.Instance.CardData,one);
				ShopCardList.Add (new CardBase(_card));
				_card = CardManager.Instance.GetCardDataFromId (DataManager.Instance.CardData,two);
				ShopCardList.Add (new CardBase(_card));
				_card = CardManager.Instance.GetCardDataFromId (DataManager.Instance.CardData,tree);
				ShopCardList.Add (new CardBase(_card));
				_card = CardManager.Instance.GetCardDataFromId (DataManager.Instance.CardData,foure);
				ShopCardList.Add (new CardBase(_card));
				_card = CardManager.Instance.GetCardDataFromId (DataManager.Instance.CardData,five);
				ShopCardList.Add (new CardBase(_card));
				Debug.LogWarning("name:"+_card.CardName);

		}



		/// <summary>
		/// カード購入時の挙動
		/// </summary>
		public void BuyCardListItem(){

				m_PhotonView.RPC("BuyCardListItemRPC",PhotonTargets.All);


		}
		[RPC]
		public void BuyCardListItemRPC(){
				ProductionManager.Instance.AppendScrollListWindow ("カード購入:所持金"+GameManager.Instance.m_CurrentPlayer.Money+"G",
						AppendCardListSaleItem);
				//かぶり順をあげる処理
				ProductionManager.Instance.m_CurrentListWindow.AddUIPanelDepth();
		}

		/// <summary>
		/// カード購入時処理
		/// 現在プレイヤーのカードリスト生成用関数
		/// </summary>
		private void AppendCardListSaleItem(){
				ScrollListWindowController _Controller = ProductionManager.Instance.m_CurrentListWindow;
				for(int i=0;ShopCardList.Count>i;i++){
						GameObject _Item = Instantiate (_Controller.m_ListItem) as GameObject;
						ListItemController _ItemController = _Item.GetComponent<ListItemController> ();
						_Item.transform.parent = _Controller.m_UIGrid.transform;
						_Item.transform.localScale = Vector3.one;
						_Item.name = ""+ShopCardList[i].CardId;
						//_ItemController.ListItemCallBack = onClickListSaleItem;
						_ItemController.NameLabel.text = ShopCardList[i].CardName+":"+ShopCardList[i].CardPrice+"G";
				}
				_Controller.m_UIGrid.Reposition ();
				WindowManager.Instance.ListItemCallBack = onClickListSaleItem;
		}

		/// <summary>
		/// カード購入時処理
		/// カードリストアイテム売却時の挙動を登録
		/// </summary>
		public void onClickListSaleItem(){

				m_PhotonView.RPC("onClickListSaleItemRPC",PhotonTargets.All,UIButton.current.name);

		}

		[RPC]
		public void onClickListSaleItemRPC(string UIButtonName){
				Debug.Log ("onClickListSaleItemRPC");
				if (GameManager.Instance.m_CurrentPlayer.m_CardList.Count >= StaticData.MAX_CARD_NUM) {//カードをこれ以上持てない場合
						ProductionManager.Instance.AppendPopUp (true, "これ以上カードを持てません");
				} else {


						CardBase _card = CardManager.Instance.GetCardDataFromId (ShopCardList, int.Parse (UIButtonName));
						Debug.Log (_card.CardName);
						Debug.Log (GameManager.Instance.m_CurrentPlayer.Money);
						if (_card.CardPrice <= GameManager.Instance.m_CurrentPlayer.Money) {
								GameManager.Instance.m_CurrentPlayer.Money -= _card.CardPrice;
								GameManager.Instance.m_CurrentPlayer.m_CardList.Add (_card);
								AudioManager.Instance.PlaySE (0);

						} else {
								AudioManager.Instance.PlaySE (1);
								ProductionManager.Instance.AppendPopUp (true, "お金がたりません");
								//UIPanelを一つ上に表示
								ProductionManager.Instance.m_CurrentPopUp.AddUIPanelDepth (15);
						}
				}

				ProductionManager.Instance.m_CurrentListWindow.HideMenu ();

		}
		/*----------------------------------------------*/
		/*----------------------------------------------*/

		/// <summary>
		/// カード売却時の挙動
		/// </summary>
		public void SaleCardListItem(){
				m_PhotonView.RPC ("SaleCardListItemRPC", PhotonTargets.All);
		}

		[RPC]
		private void SaleCardListItemRPC(){
				Debug.Log ("SaleCardListItemRPC");
				ProductionManager.Instance.AppendScrollListWindow ("カード売却",AppendCardListItem);
				//かぶり順をあげる処理
				ProductionManager.Instance.m_CurrentListWindow.AddUIPanelDepth();
		}


		/// <summary>
		/// カード売却時処理
		/// 現在プレイヤーのカードリスト生成用関数
		/// </summary>
		private void AppendCardListItem(){
				ScrollListWindowController _Controller = ProductionManager.Instance.m_CurrentListWindow;
				for(int i=0;GameManager.Instance.m_CurrentPlayer.m_CardList.Count>i;i++){
						GameObject _Item = Instantiate (_Controller.m_ListItem) as GameObject;
						ListItemController _ItemController = _Item.GetComponent<ListItemController> ();
						_Item.transform.parent = _Controller.m_UIGrid.transform;
						_Item.transform.localScale = Vector3.one;
						_Item.name = ""+GameManager.Instance.m_CurrentPlayer.m_CardList[i].CardId;

						Debug.Log (GameManager.Instance.m_CurrentPlayer.m_CardList [i].CardName);
						_ItemController.NameLabel.text = GameManager.Instance.m_CurrentPlayer.m_CardList [i].CardName+":"+(GameManager.Instance.m_CurrentPlayer.m_CardList [i].CardPrice)/2+"G";
				}
				_Controller.m_UIGrid.Reposition ();
				WindowManager.Instance.ListItemCallBack = onClickListCardItem;
		}

		/// <summary>
		/// カード売却時処理
		/// カードリストアイテム売却時の挙動を登録
		/// </summary>
		private void onClickListCardItem(){
				
				Debug.Log ("onClickListCardItem");
				m_PhotonView.RPC("onClickListCardItemRPC",PhotonTargets.All,UIButton.current.name);

		}

		[RPC]
		private void onClickListCardItemRPC(string UIButtonName){
				
				Debug.Log ("onClickListCardItemRPC"+UIButtonName);
				CardBase _card = CardManager.Instance.GetCardDataFromId (GameManager.Instance.m_CurrentPlayer.m_CardList,int.Parse(UIButtonName));
				GameManager.Instance.m_CurrentPlayer.Money += _card.CardPrice / 2;
				GameManager.Instance.m_CurrentPlayer.m_CardList.Remove (_card);
				ProductionManager.Instance.m_CurrentListWindow.HideMenu ();
				//リストを再表示する
				//SaleCardListItem ();

		}

		/*----------------------------------------------*/
		/*----------------------------------------------*/


		/// <summary>
		/// 銀行のイベント
		/// 特定条件がそろったときのみイベントが発動する
		/// Coefficientは得点の倍率
		/// </summary>
		/// <param name="targetPlayer">Target player.</param>
		/// <param name="Coefficient">Coefficient.</param>
		public void BankEvent(Player targetPlayer,float Coefficient){
				if (targetPlayer.MarkList.Count == 4) {//プレイヤーがすべてのマークを集め終わっている場合
						int ArticleMoney =0;
						for(int i=0;targetPlayer.ArticleList.Count>i;i++){
								ArticleMoney += targetPlayer.ArticleList [i].PropertyTax;
						}
						int money = (int)((float)(targetPlayer.NumberOfGoing * 500 + ArticleMoney) * Coefficient);
						targetPlayer.Money += money;

						//markリストの初期化
						targetPlayer.MarkList = new List<int>();
						//windowのマーク表示の初期化
						GameManager.Instance.initPlayerMark (targetPlayer);
						//目的地リストの初期化
						targetPlayer.DestinationList = new List<TileBase> ();
						//演出
						ProductionManager.Instance.AppendBankFx(GameManager.Instance.m_CurrentTile.gameObject,money);
						//周回数加算
						targetPlayer.NumberOfGoing++;
				}
		}

		/// <summary>
		/// 物件増資
		/// </summary>
		private void CapitalIncrease(){
				if (GameManager.Instance.m_CurrentPlayer.Money < GameManager.Instance.m_CurrentTile.PropertyPrice) {//お金が足りない場合
						ProductionManager.Instance.AppendPopUp(true,"お金がたりません");
						//UIPanelを一つ上に表示
						ProductionManager.Instance.m_CurrentPopUp.AddUIPanelDepth ();
				} else {
						

						GameManager.Instance.m_CurrentTile.PropertyPrice += GameManager.Instance.m_CurrentTile.PropertyPrice;
						GameManager.Instance.m_CurrentTile.PropertyTax += GameManager.Instance.m_CurrentTile.PropertyTax;

						GameManager.Instance.m_CurrentTile.TaxLabel.text = "" + GameManager.Instance.m_CurrentTile.PropertyTax;
						GameManager.Instance.m_CurrentTile.MoneyLabel.text = "" + GameManager.Instance.m_CurrentTile.PropertyPrice;

						GameManager.Instance.m_CurrentTile.PropertyRanck++;

						ProductionManager.Instance.m_CurrentPopUp.HideMenu ();
						ProductionManager.Instance.AppendPopUp(true,"物件を増資しました");
						GameManager.Instance.m_CurrentPlayer.Money -= GameManager.Instance.m_CurrentTile.PropertyPrice;
						//取り急ぎターン進行処理を入れる
						GameManager.Instance.TurnCycleRPC ();

				}
		}


		/// <summary>
		///物件購入
		/// </summary>
		private void BuyProperty(){
				if (GameManager.Instance.m_CurrentPlayer.Money < GameManager.Instance.m_CurrentTile.PropertyPrice) {//お金が足りない場合
						ProductionManager.Instance.AppendPopUp(true,"お金がたりません");
						//UIPanelを一つ上に表示
						ProductionManager.Instance.m_CurrentPopUp.AddUIPanelDepth ();
				} else {
						GameManager.Instance.m_CurrentTile.HolderNumber = GameManager.Instance.m_CurrentPlayer.PlayerNumber;
						GameManager.Instance.m_CurrentTile.SetTileBg (GameManager.Instance.m_CurrentPlayer.PlayerNumber);
						GameManager.Instance.m_CurrentPlayer.ArticleList.Add (GameManager.Instance.m_CurrentTile);
						ProductionManager.Instance.m_CurrentPopUp.HideMenu ();
						ProductionManager.Instance.AppendPopUp(true,"物件を購入しました");
						GameManager.Instance.m_CurrentPlayer.Money -= GameManager.Instance.m_CurrentTile.PropertyPrice;
						//取り急ぎターン進行処理を入れる
						GameManager.Instance.TurnCycleRPC ();

				}

		}

		private void ClosePopUp(){
				Debug.Log ("ClosePopUp");
				//取り急ぎターン進行処理を入れる
				GameManager.Instance.TurnCycleRPC ();
				ProductionManager.Instance.m_CurrentPopUp.HideMenu ();
		}

		/// <summary>
		/// 現在プレイヤーがランダムにカードを取得する
		/// </summary>
		public void GetRandomCard(){
				//int RandomCount = Random.Range (0, DataManager.Instance.CardData.Count);
				GameManager.Instance.m_CurrentPlayer.m_CardList.Add (new CardBase(DataManager.Instance.CardData[GameManager.Instance.m_CurrentPlayer.RandomCardNumber]));
				ProductionManager.Instance.AppendCardFx (GameManager.Instance.m_CurrentTile.gameObject);
		}
}
