﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : SingletonMonoBehaviour<GameManager> {
	
		/// <summary>
		/// ゲーム内の全プレイヤー情報を格納
		/// </summary>
		public List<Player> m_Players;
		/// <summary>
		/// 現在操作中のプレイヤー情報を格納
		/// </summary>
		public Player m_CurrentPlayer;
		public int m_CurrentPlayerNumber;
		/// <summary>
		/// プレイヤーオブジェクトのコンテイナー
		/// </summary>
		[SerializeField]
		public GameObject m_PlayerContainer;
		/// <summary>
		/// ターン進行数
		/// </summary>
		public int TrunCount;
		/// <summary>
		/// サイコロの演出オブジェクト
		/// DiceNumber に表示したい目の番号をセットする
		/// </summary>
		public GameObject m_Dice;
		/// <summary>
		/// 現在イベント発生中のタイル情報をキャッシュ
		/// </summary>
		public TileBase m_CurrentTile;
		/// <summary>
		/// プレイヤーのステータスを配列に格納
		/// </summary>
		public List<PlayerStatusWindowCotroller> PlayerStatusWindowCotrollerList;

		/// <summary>
		/// スクロールビュー
		/// </summary>
		[SerializeField]
		private UIScrollView m_ScrollView;
		[SerializeField]
		private UIPanel m_ScrollUIPanel;
		/// <summary>
		/// 3DMapオブジェクト
		/// </summary>
		[SerializeField]
		private GameObject m_Map;

		/// <summary>
		/// 要素をセンタリングするためのスクリプト
		/// </summary>
		[SerializeField]
		private UICenterOnChild m_UICenterOnChild;
		/// <summary>
		/// ゲームクリアに必要な設定金額
		/// (銀行のマスに記載)
		/// </summary>
		public int ClearMoney;
		/// <summary>
		/// 現在ホバー状態のリストアイテム名
		/// </summary>
		public string m_CurrentHoverListItemName;
		/// <summary>
		/// ゲーム終了時演出
		/// </summary>
		[SerializeField]
		private GameObject m_GameEndWindow;
		/// <summary>
		/// 部屋一覧window
		/// </summary>
		[SerializeField]
		private GameObject m_PhotonRoomWindow;
		/// <summary>
		/// マッチングwindow
		/// </summary>
		[SerializeField]
		private GameObject m_MatchingWindow;
		/// <summary>
		/// プレイヤーの同期通信開始か否か
		/// </summary>
		public bool isLeadyPlayerSync;
		/// <summary>
		/// The m photon view.
		/// </summary>
		[SerializeField]
		private PhotonView m_PhotonView;


		[SerializeField]
		private GameObject m_PlayerObject;
		[SerializeField]
		private GameObject m_PlayerStatus;
		[SerializeField]
		public UIGrid m_PlayerStatusContainer;

		[SerializeField]
		private Player[] Computers;

		void Awake(){
				//testaaaa
				isLeadyPlayerSync = false;
				PlayerStatusWindowCotrollerList = new List<PlayerStatusWindowCotroller> ();
		}
		void Start () {
				//部屋windowの表示
				m_PhotonRoomWindow.SetActive (true);
				m_MatchingWindow.SetActive (true);
		}

		/// <summary>
		/// ゲームの開始処理
		/// </summary>
		public void StartGameRPC ()
		{
				m_PhotonView.RPC ("StartGame", PhotonTargets.All);
		}

		[RPC]
		public void StartGame(){
				Debug.Log ("StartGame");
				m_MatchingWindow.gameObject.SetActive (false);

				m_Players.Sort (PlayerListSortKey);

				//GameManager.Instance.m_Players = new List<Player> (PhotonManager.Instance.m_PhotonPlayerList);
				m_CurrentPlayer = m_Players [0];
				if (!m_CurrentPlayer.isPlayer) {//現在プレイヤーが操作プレイヤーでないとき
						//思考表示
						ProductionManager.Instance.SetLoadingWindow (true);
				}
				m_PlayerStatusContainer.Reposition ();

				//---------

				//コンピューターの追加処理
				if (m_Players.Count >= 2) {
				} else {//プレイヤーが一人のときのみコンピューターを出す
						addComputer ();
				}


				initGame ();

				m_CurrentTile = TileManager.Instance.m_BankTile;
				SetActivePlayer ();
				initPlayerPosition ();
				//ゲーム開始時演出
				StartCoroutine (GameStartProduction ());
				isLeadyPlayerSync = true;

				//スクロールのデリゲートを登録
				//m_ScrollView.onMomentumMove = SyncMapPosFromScroll;
		}

		/// <summary>
		/// プレイヤーリストのソート用関数
		/// </summary>
		public int PlayerListSortKey (Player x, Player y)
		{
				// 第一のキーで比較
				if (x.PlayerNumber > y.PlayerNumber) {
						return 1;
				} else if (x.PlayerNumber < y.PlayerNumber) {
						return -1;
				}
				return 0;
		}

		/// <summary>
		/// コンピューターの追加処理
		/// </summary>
		private void addComputer(){
				//コンピューターの生成処理

				int count = 3 - m_Players.Count;

				for(int i=0;count>i;i++){
						


						Computers [i].gameObject.SetActive (true);
						m_Players.Add (Computers [i]);


						//----ステータスウィンドのInstantiate
						GameObject _window = Instantiate (m_PlayerStatus) as GameObject;
						PlayerStatusWindowCotroller _cont = _window.GetComponent<PlayerStatusWindowCotroller> ();
						GameManager.Instance.PlayerStatusWindowCotrollerList.Add (_cont);
						_cont.m_Player = Computers [i];
						_window.transform.parent = m_PlayerStatusContainer.transform;
						_window.transform.localScale  = Vector3.one;
				}

				m_PlayerStatusContainer.Reposition ();

		}


		/// <summary>
		/// ゲームスタート時演出
		/// </summary>
		private IEnumerator GameStartProduction(){
				//現在プレイヤー名の表示演出
				ProductionManager.Instance.AppendTextFx ("Game Start!!!");
				yield return new WaitForSeconds (1.4f);
				//現在プレイヤー名の表示演出
				ProductionManager.Instance.AppendTextFx (m_CurrentPlayer.UserName+"のターン!");
				yield return new WaitForSeconds (1.4f);
				/*
				if(m_CurrentPlayer.isComputer){//一番手のプレイヤーがコンピューターの場合
						TurnCycleRPC();
				}
				*/
		}

		private void GameEndProduction(){
				m_GameEndWindow.SetActive (true);
		}


		/// <summary>
		/// Gameの初期化
		/// </summary>
		private void initGame(){
				TrunCount = 0;
				m_CurrentPlayer = m_Players [0];
				m_GameEndWindow.SetActive (false);
				m_CurrentPlayerNumber = 0;

		}

		/// <summary>
		/// スクロールの座標とMAPの座標をシンクロさせる
		/// </summary>
		public void SyncMapPosFromScroll(){
				Vector3 mapPos = m_Map.transform.localPosition;
				m_Map.transform.localPosition = new Vector3 (
						m_ScrollView.transform.localPosition.x/100.0f,
						m_ScrollView.transform.localPosition.y/100.0f,
						mapPos.z
				);
		}

		/// <summary>
		/// プレイヤーの初期位置を銀行にする
		/// </summary>
		private void initPlayerPosition (){
				//プレイヤーの初期位置を銀行にする
				for(int i=0;m_Players.Count>i;i++){
						m_Players [i].transform.position = TileManager.Instance.m_BankTile.transform.position;
						m_Players [i].ResetPosition ();
				}
		}

		/// <summary>
		/// 現在プレイヤーのdepthとカーソルをセットする
		/// </summary>
		private void SetActivePlayer(){
				


				for(int i=0;m_Players.Count>i;i++){
						m_Players [i].m_CharaSprite.depth = 26;
						m_Players [i].m_Curser.SetActive (false);
				}
				m_CurrentPlayer.m_CharaSprite.depth = 27;
				m_CurrentPlayer.m_Curser.SetActive (true);




		}

		/// <summary>
		/// プレイヤー名表示時にセンタリングすべし
		/// スクロール座標をセンタリングする
		/// </summary>
		public void CenteringScroll(){
				m_UICenterOnChild.CenterOn (m_CurrentPlayer.transform);
		}

		/*-------------------物件売却---------------------------*/
		/*----------------------------------------------*/

		/// <summary>
		/// 所持金がマイナスになったときに手持ちの物件を所持金がプラスになるまで売る処理
		/// 
		/// </summary>
		public IEnumerator SalePropertyListItem(){



				ProductionManager.Instance.AppendScrollListWindow ("物件売却"+"\n"+"所持金:"+m_CurrentPlayer.Money+"G",AppendPropertyListItem);
				//閉じるボタンの非表示
				ProductionManager.Instance.m_CurrentListWindow.m_NoButton.gameObject.SetActive (false);
				//かぶり順をあげる処理
				ProductionManager.Instance.m_CurrentListWindow.AddUIPanelDepth();
				//説明文言のセット
				ProductionManager.Instance.m_CurrentListWindow.SetSubWindowText ("お金がたりないです＞＜"+"\n"+"\n"+"何か物件を売らなくちゃ");
				if(m_CurrentPlayer.isComputer){//コンピューターの場合
						yield return new WaitForSeconds(1.0f);
						//売却処理
						//UIButton.current.name = ProductionManager.Instance.m_CurrentListWindow.m_ItemList[0].gameObject.name;
						//ProductionManager.Instance.m_CurrentListWindow.m_ItemList[0].GetComponent<UIButton>().onClick[0].Execute(onClick);
						//EventDelegate.Execute (ProductionManager.Instance.m_CurrentListWindow.m_ItemList[0].GetComponent<UIButton>().onClick);
						UIButton.current = ProductionManager.Instance.m_CurrentListWindow.m_ItemList[0].GetComponent<UIButton>();
						ProductionManager.Instance.m_CurrentListWindow.m_ItemList[0].onClickListItemButton();
				}


		}
		/// <summary>
		/// 物件売却時処理
		/// 現在プレイヤーのカードリスト生成用関数
		/// </summary>
		private void AppendPropertyListItem(){
				ScrollListWindowController _Controller = ProductionManager.Instance.m_CurrentListWindow;
				for(int i=0;GameManager.Instance.m_CurrentPlayer.ArticleList.Count>i;i++){
						GameObject _Item = Instantiate (_Controller.m_ListItem) as GameObject;
						ListItemController _ItemController = _Item.GetComponent<ListItemController> ();
						ProductionManager.Instance.m_CurrentListWindow.m_ItemList.Add (_ItemController);
						_Item.transform.parent = _Controller.m_UIGrid.transform;
						_Item.transform.localScale = Vector3.one;
						//リスト番号/物件価格/2
						_Item.name = i+"/"+(GameManager.Instance.m_CurrentPlayer.ArticleList[i].PropertyPrice)/2;

						_ItemController.NameLabel.text = GameManager.Instance.m_CurrentPlayer.ArticleList[i].PropertyName+":"+(GameManager.Instance.m_CurrentPlayer.ArticleList[i].PropertyPrice)/2+"G";
				}
				_Controller.m_UIGrid.Reposition ();
				WindowManager.Instance.ListItemCallBack = onClickListPropertyItem;
		}

		/// <summary>
		/// 物件売却時処理
		/// カードリストアイテム売却時の挙動を登録
		/// </summary>
		private void onClickListPropertyItem(){



				m_PhotonView.RPC("onClickListPropertyItemRPC",PhotonTargets.All,UIButton.current.name);

		}
		[RPC]
		private void onClickListPropertyItemRPC(string UIButtonName){
				//リスト番号/売却価格
				string[] data = UIButtonName.Split ('/');
				GameManager.Instance.m_CurrentPlayer.Money += int.Parse(data[1]);
				GameManager.Instance.m_CurrentPlayer.ArticleList.RemoveAt (int.Parse(data[0]));


				if (GameManager.Instance.m_CurrentPlayer.Money < 0 && GameManager.Instance.m_CurrentPlayer.ArticleList.Count>=1) {
						//-------------再処理
						ProductionManager.Instance.m_CurrentListWindow.HideAndTurnEnd ();
						//リストを再表示する
						StartCoroutine(
								SalePropertyListItem ());
				} else {
						//-------------終了
						ProductionManager.Instance.m_CurrentListWindow.HideAndTurnEnd ();
				}
		}


		/*----------------------物件売却------------------------*/
		/*----------------------------------------------*/



		/// <summary>
		/// プレイヤーのあたり判定のON OFF
		/// </summary>
		public void SetPlayersCollider(bool isEnable){
				for(int i=0;m_Players.Count>i;i++){
						m_Players [i].GetComponent<BoxCollider>().enabled = isEnable;
				}
				
		}

		/*
		/// <summary>
		/// プレイヤーがカードリスト表示ボタンを押したときの挙動
		/// </summary>
		public void onClickCardListButton(){//windowManagerに機能を移行
				ProductionManager.Instance.AppendScrollListWindow ("カード一覧",AppendCardListItem);
				ProductionManager.Instance.m_CurrentListWindow.SetSubWindowText ("使いたいカードを選択してね");
		}
		*/

		/// <summary>
		/// Windowとプレイヤーのステータスをシンクロさせる
		/// </summary>
		public void SyncPlayerStatus(){
				for(int i=0;PlayerStatusWindowCotrollerList.Count>i;i++){
						PlayerStatusWindowCotrollerList [i].SyncPlayerStatu ();
				}
		}

		/// <summary>
		/// 対象プレイヤーのMarkを初期化する
		/// </summary>
		public void initPlayerMark(Player target){
				for(int i=0;PlayerStatusWindowCotrollerList.Count>i;i++){
						if(PlayerStatusWindowCotrollerList [i].m_Player == target){
								PlayerStatusWindowCotrollerList [i].initMarkSpriteColor ();
						}
				}
		}


		/// <summary>
		/// 現在プレイヤーのリスト生成用関数
		/// </summary>
		public void AppendCardListItem(){
				ScrollListWindowController _Controller = ProductionManager.Instance.m_CurrentListWindow;
				for(int i=0;m_CurrentPlayer.m_CardList.Count>i;i++){
						
						GameObject _Item = Instantiate (_Controller.m_ListItem) as GameObject;
						ListItemController _ItemController = _Item.GetComponent<ListItemController> ();
						_Item.transform.parent = _Controller.m_UIGrid.transform;
						_Item.transform.localScale = Vector3.one;
						_Item.name = ""+m_CurrentPlayer.m_CardList[i].CardId;

						_ItemController.ListItemHoverCallBack = onHoverListCardItem;
						Debug.Log (m_CurrentPlayer.m_CardList [i].CardName);
						_ItemController.NameLabel.text = m_CurrentPlayer.m_CardList [i].CardName;
				}
				_Controller.m_UIGrid.Reposition ();
				WindowManager.Instance.ListItemCallBack = onClickListCardItem;
		}

		/// <summary>
		/// カードリストアイテム押下時の挙動を登録
		/// </summary>
		private void onClickListCardItem(){
				m_PhotonView.RPC("onClickListCardItemRPC",PhotonTargets.All,UIButton.current.name);
		}

		[RPC]
		private void onClickListCardItemRPC(string UIButtonName){
				CardManager.Instance.UseCard (int.Parse(UIButtonName));
				CardBase _card = CardManager.Instance.GetCardDataFromId (m_CurrentPlayer.m_CardList,int.Parse(UIButtonName));
				m_CurrentPlayer.m_CardList.Remove (_card);
				ProductionManager.Instance.m_CurrentListWindow.HideMenu ();
				Debug.Log (UIButtonName);
		}



		/// <summary>
		/// カードリストホバー時の挙動
		/// </summary>
		private void onHoverListCardItem(){
				CardBase _card = CardManager.Instance.GetCardDataFromId (m_CurrentPlayer.m_CardList,int.Parse(m_CurrentHoverListItemName));
				ProductionManager.Instance.m_CurrentListWindow.SetSubWindowText (_card.CardNote);
				Debug.Log ("onHover");
		}


		/// <summary>
		/// プレイヤー番号からプレイヤーオブジェクトを取得する
		/// </summary>
		/// <returns>The player from player number.</returns>
		/// <param name="PlayerNumber">Player number.</param>
		public Player GetPlayerFromPlayerNumber(int PlayerNumber){
				for(int i=0;m_Players.Count>i;i++){
						if (m_Players [i].PlayerNumber == PlayerNumber)
								return m_Players [i];
				}
				return m_Players [0];
		}
		/// <summary>
		/// 現在プレイヤーの移動可能範囲の割り出し
		/// </summary>
		public void CalculationCurrentPlayer(){
				m_CurrentPlayer.CalculationRemovableArea ();
		}
		/// <summary>
		/// 現在プレイヤーがサイコロを振る
		/// </summary>
		public void SetDiceNumberFromCurrentPlayer(){
				//m_CurrentPlayer.SetDiceNumber ();
				int MoveCount = UnityEngine.Random.Range (1, 7);
				m_CurrentPlayer.MoveCount = MoveCount;
				CalculationCurrentPlayer ();
				DiceProduction (MoveCount);
		}

		//サイコロ演出
		private void DiceProduction(int MoveCount){
				m_PhotonView.RPC ("DiceProductionRPC", PhotonTargets.All,MoveCount);
		}

		[RPC]
		private void DiceProductionRPC(int MoveCount){
				m_CurrentPlayer.MoveCount = MoveCount;
				ProductionManager.Instance.AppendDice (m_CurrentPlayer.MoveCount);
		}

		/// <summary>
		/// 現在プレイヤーの総資産を計算する
		/// </summary>
		private void TotalAssetsCalc(){
				int PropertyPrice=0;
				for(int i=0;m_CurrentPlayer.ArticleList.Count>i;i++){
						PropertyPrice += m_CurrentPlayer.ArticleList [i].PropertyPrice;
				}

				m_CurrentPlayer.TotalAssets = m_CurrentPlayer.Money+PropertyPrice;
		}

		/// <summary>
		/// ゲームの開始処理
		/// </summary>
		public void TurnCycleRPC ()
		{
				//m_PhotonView.RPC ("TurnCycle", PhotonTargets.All);

				//------------

				//ゲーム開始時でないときはリターンする
				if (!isLeadyPlayerSync)
						return;

				Debug.Log ("TurnCycle");
				//コンピューター思考表示の解除
				ProductionManager.Instance.SetLoadingWindow (false);
				//yield return new WaitForSeconds(0.2f);
				//全プレイヤーのステータスをシンクロ
				SyncPlayerStatus ();
				//現在プレイヤーの総資産を計算する
				TotalAssetsCalc ();
				//ゲーム終了かどうか判定
				if (m_CurrentPlayer.TotalAssets >= ClearMoney) {
						//ゲーム終了処理
						GameEndProduction ();
						return;
				}


				if (GameManager.Instance.m_CurrentPlayer.ArticleList.Count > 0 && GameManager.Instance.m_CurrentPlayer.Money < 0) {
						//所持金がマイナスの場合で物件を所持している場合は物件売却windowを表示する
						StartCoroutine(
								SalePropertyListItem ());

				} else {

						//現在プレイヤーの切り替え
						SetCurrentPlayer ();
						if (!m_CurrentPlayer.isPlayer) {//現在プレイヤーが操作プレイヤーでないとき
								//思考表示
								ProductionManager.Instance.SetLoadingWindow (true);
						}
						//yield return new WaitForSeconds(0.4f);
						//現在プレイヤー名の表示演出
						ProductionManager.Instance.AppendTextFx (m_CurrentPlayer.UserName + "のターン!");
						//yield return new WaitForSeconds(0.6f);
						//現在プレイヤーがコンピューターの場合は思考AIを起動する
						if (m_CurrentPlayer.isComputer) {
								
								StartCoroutine (AIManager.Instance.AI ());
						}
				}

		}

		/// <summary>
		/// ゲームのターン進行用
		/// </summary>
		[RPC]
		public void TurnCycle(){
				
				//ゲーム開始時でないときはリターンする
				if (!isLeadyPlayerSync)
						return;

				Debug.Log ("TurnCycle");
				//コンピューター思考表示の解除
				ProductionManager.Instance.SetLoadingWindow (false);


				//yield return new WaitForSeconds(0.2f);
				//全プレイヤーのステータスをシンクロ
				SyncPlayerStatus ();
				//現在プレイヤーの総資産を計算する
				TotalAssetsCalc ();
				//ゲーム終了かどうか判定
				if (m_CurrentPlayer.TotalAssets >= ClearMoney) {
						//ゲーム終了処理
						GameEndProduction ();
						return;
				}


				if (GameManager.Instance.m_CurrentPlayer.ArticleList.Count > 0 && GameManager.Instance.m_CurrentPlayer.Money < 0) {
						//所持金がマイナスの場合で物件を所持している場合は物件売却windowを表示する
						StartCoroutine(
								SalePropertyListItem ());

				} else {

						//現在プレイヤーの切り替え
						SetCurrentPlayer ();

						if (!m_CurrentPlayer.isPlayer) {//現在プレイヤーが操作キャラでない場合
								//思考中windowの表示
								ProductionManager.Instance.SetLoadingWindow (true);
						}

						//yield return new WaitForSeconds(0.4f);
						//現在プレイヤー名の表示演出
						ProductionManager.Instance.AppendTextFx (m_CurrentPlayer.UserName + "のターン!");
						//yield return new WaitForSeconds(0.6f);
						//現在プレイヤーがコンピューターの場合は思考AIを起動する
						if (m_CurrentPlayer.isComputer && PhotonNetwork.isMasterClient) {
								
								StartCoroutine (AIManager.Instance.AI ());
						}
				}

		}

		/// <summary>
		/// 現在プレイヤーと番号をセットする
		/// </summary>
		private void SetCurrentPlayer(){
				if (m_CurrentPlayerNumber == m_Players.Count-1) {
						m_CurrentPlayerNumber = 0;
						TrunCount++;
				} else {
						m_CurrentPlayerNumber++;
				}
				m_CurrentPlayer = m_Players [m_CurrentPlayerNumber];
				//キャラの表示をアップデート
				SetActivePlayer ();
		}

}
