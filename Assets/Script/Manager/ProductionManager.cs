﻿using UnityEngine;
using System.Collections;
using System;
/// <summary>
/// 演出やwindowオブジェクトの生成を行う
/// </summary>
public class ProductionManager : SingletonMonoBehaviour<ProductionManager> {
		[SerializeField]
		private GameObject m_TextFx;
		[SerializeField]
		private GameObject m_PopUp;
		[SerializeField]
		private GameObject m_MarkFx;
		[SerializeField]
		private GameObject m_CardFx;
		[SerializeField]
		private GameObject m_TaxFx;

		[SerializeField]
		private GameObject m_WindowParent;
		[SerializeField]
		private GameObject m_ScrollListWindow;
		[SerializeField]
		private GameObject m_CardShopWindow;
		[SerializeField]
		private GameObject m_Dice;
		[SerializeField]
		private GameObject m_LoadingWindow;
		/// <summary>
		/// 現在展開中のポップアップ情報をキャッシュ
		/// </summary>
		public PopUpController m_CurrentPopUp;
		/// <summary>
		/// 現在展開中のリストウィンドウ情報をキャッシュ
		/// </summary>
		public ScrollListWindowController m_CurrentListWindow;

	// Use this for initialization
	void Start () {
	}
		/// <summary>
		/// サイコロをふる演出
		/// 引数に応じてサイコロの目が出る
		/// </summary>
		/// <param name="Number">Number.</param>
		public void AppendDice(int Number){
				GameObject _Dice = Instantiate (m_Dice) as GameObject;
				_Dice.GetComponent<DiceController> ().DiceNumber = Number;
		}

		public void SetLoadingWindow(bool Active){
				Debug.Log ("SetLoadingWindow:"+Active);
				m_LoadingWindow.SetActive (Active);
		}

		public void AppendTextFx(string TextBody){
				GameObject obj = Instantiate (m_TextFx) as GameObject;
				TextFxController _Controller = obj.GetComponent<TextFxController> ();
				_Controller.m_Label.text = TextBody;
				obj.transform.parent = m_WindowParent.transform;
				obj.transform.localScale = Vector3.one;
		}

		public void AppendPopUp(bool isPopUp,string TextBody){
				Debug.Log ("AppendPopUp");
				GameObject obj = Instantiate (m_PopUp) as GameObject;
				PopUpController _Controller = obj.GetComponent<PopUpController> ();
				m_CurrentPopUp = _Controller;
				_Controller.m_PopUpLabel.text = TextBody;
				_Controller.isPopUp = isPopUp;
				obj.transform.parent = m_WindowParent.transform;
				obj.transform.localScale = Vector3.one;
		}
		/// <summary>
		/// スクロールリストアイテムを生成
		/// </summary>
		/// <param name="TextBody">Text body.</param>
		/// <param name="onClickListItem">On click list item.</param>
		public void AppendScrollListWindow(string TextBody,Action AppendListItems){
				GameObject obj = Instantiate (m_ScrollListWindow) as GameObject;
				ScrollListWindowController _Controller = obj.GetComponent<ScrollListWindowController> ();
				m_CurrentListWindow = _Controller;
				_Controller.m_PopUpLabel.text = TextBody;
				//リストアイテム生成時のコールバック
				_Controller.AppendListItems = AppendListItems;
				obj.transform.parent = m_WindowParent.transform;
				obj.transform.localScale = Vector3.one;
		}
		public void AppendCardShopWindow(){
				GameObject obj = Instantiate (m_CardShopWindow) as GameObject;
				obj.transform.parent = m_WindowParent.transform;
				ScrollListWindowController _Controller = obj.GetComponent<ScrollListWindowController> ();
				m_CurrentListWindow = _Controller;
				obj.transform.localScale = Vector3.one;
		}

		public void AppendMarkFx(int KindofTile,GameObject targetTile){
				GameObject obj = Instantiate (m_MarkFx) as GameObject;
				MarkFxController _Controller = obj.GetComponent<MarkFxController> ();
				_Controller.SetMarkSprite (KindofTile);
				obj.transform.parent = targetTile.transform;
				obj.transform.localPosition = Vector3.zero;
				obj.transform.localScale = Vector3.one;
		}
		public void AppendCardFx(GameObject targetTile){
				GameObject obj = Instantiate (m_CardFx) as GameObject;
				MarkFxController _Controller = obj.GetComponent<MarkFxController> ();
				obj.transform.parent = targetTile.transform;
				obj.transform.localPosition = Vector3.zero;
				obj.transform.localScale = Vector3.one;
		}
		public void AppendTaxFx(GameObject targetTile,string playerName,string targetName,int money){
				GameObject obj = Instantiate (m_TaxFx) as GameObject;
				MarkFxController _Controller = obj.GetComponent<MarkFxController> ();
				_Controller.SetTaxFx (playerName,targetName,money);
				obj.transform.parent = targetTile.transform;
				obj.transform.localPosition = Vector3.zero;
				obj.transform.localScale = Vector3.one;
		}
		/// <summary>
		/// 銀行演出
		/// </summary>
		/// <param name="money">Money.</param>
		public void AppendBankFx(GameObject targetTile,int money){
				GameObject obj = Instantiate (m_TaxFx) as GameObject;
				MarkFxController _Controller = obj.GetComponent<MarkFxController> ();
				_Controller.SetBankFx (money);
				obj.transform.parent = targetTile.transform;
				obj.transform.localPosition = Vector3.zero;
				obj.transform.localScale = Vector3.one;
		}
	
	
}
