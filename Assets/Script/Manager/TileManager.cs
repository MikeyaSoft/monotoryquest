﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TileManager : SingletonMonoBehaviour<TileManager> {
	
		[SerializeField]
		private UIGrid TileContainaer;
		[SerializeField]
		private GameObject m_Tile;
		public List<TileBase> TileList;
		/// <summary>
		/// 分岐点リスト
		/// </summary>
		public List<TileBase> TurningPointList;
		/// <summary>
		/// 現在判定中のタイル情報をキャッシュする
		/// </summary>
		private TileBase CurrentMoveTile;
		/// <summary>
		/// 分岐点のタイル情報をキャッシュ
		/// </summary>
		private TileBase MigrationPathway;
		/// <summary>
		/// 経路番号
		/// </summary>
		private int MigrationPathwayNumber;
		/// <summary>
		/// 初期フラグ
		/// </summary>
		public bool isFast;
		/// <summary>
		/// 銀行のタイル情報をキャッシュ
		/// </summary>
		public TileBase m_BankTile;
	
	// Use this for initialization
	void Start () {
				isFast = true;
				ApendTiles ();
				//GetTileList ();
	}
		/// <summary>
		/// IDからタイルを取得する
		/// </summary>
		/// <returns>The tile base from I.</returns>
		/// <param name="id">Identifier.</param>
		public TileBase GetTileBaseFromID(int id){
				for(int i=0;TileList.Count>i;i++){
						if (TileList [i].TileID == id)
								return TileList [i];
				}
				return TileList [0];
		}
	
	private void ApendTiles(){
				int count = 0;
				Debug.Log ("ApendTiles");
				TileList = new List<TileBase> ();
				TileContainaer.maxPerLine = DataManager.Instance.MapData [0].Length;

				for (int i = 0; DataManager.Instance.MapData.Count > i; i++) {
						for(int j=0;DataManager.Instance.MapData [i].Length>j;j++){

								count++;

								string[] Data = DataManager.Instance.MapData [i] [j].Split ('/');


								GameObject _Tile = Instantiate (m_Tile) as GameObject;
								TileBase _TileBase = _Tile.GetComponent<TileBase> ();
								_TileBase.TileID = count;
								_TileBase.KindOfTile = int.Parse (Data[0]);

								switch (_TileBase.KindOfTile) {
								case 1:
										_TileBase.BackGround.spriteName = "Tile_normal";
										_TileBase.PropertyPrice = int.Parse (Data[1]);
										_TileBase.MoneyLabel.gameObject.SetActive (true);
										_TileBase.MoneyLabel.text = ""+int.Parse (Data[1]); 
										_TileBase.TaxLabel.gameObject.SetActive (true);
										_TileBase.TaxLabel.text = ""+int.Parse (Data[2]); 
										_TileBase.PropertyTax = int.Parse (Data[2]);
										break
										;
								case 2:
										_TileBase.BackGround.spriteName = "Tile_Card";
										break;
								case 3:
										_TileBase.BackGround.spriteName = "Tile_Bank";
										//ゲームクリア時の金額を設定
										GameManager.Instance.ClearMoney = int.Parse (Data[1]);
										m_BankTile = _TileBase;
										break;
								case 4:
										_TileBase.BackGround.spriteName = "Tile_Hart";
										break;
								case 5:
										_TileBase.BackGround.spriteName = "Tile_Crow";
										break;
								case 6:
										_TileBase.BackGround.spriteName = "Tile_Spade";
										break;
								case 7:
										_TileBase.BackGround.spriteName = "Tile_Dia";
										break;

								}

								if (_TileBase.KindOfTile == 0) {
										_TileBase.isVisible = false;
								}
								TileList.Add (_TileBase);
								_Tile.transform.parent = TileContainaer.transform;
								_Tile.transform.localScale = Vector3.one;
								_Tile.name = (i/TileContainaer.maxPerLine)+"/"+i;

						}
				}

				/*
				for(int i=0;num>i;i++){
						GameObject _Tile = Instantiate (m_Tile) as GameObject;
						TileList.Add (_Tile.GetComponent<TileBase>());
						_Tile.transform.parent = TileContainaer.transform;
						_Tile.transform.localScale = Vector3.one;
						_Tile.name = (i/TileContainaer.maxPerLine)+"/"+i;
						if (_Tile.name == "0/3" ||_Tile.name ==  "1/13" ||_Tile.name ==  "2/23") {
								_Tile.GetComponent<TileBase> ().isVisible = false;
						}
				}
				*/
				TileContainaer.onReposition = initTiles;
				TileContainaer.Reposition ();
				isFast = false;

		}
		/// <summary>
		/// すべてのタイル情報を初期化
		/// </summary>
		public void initTiles(){
				Debug.Log ("initTiles");
				for(int i=0;TileList.Count>i;i++){
						TileList [i].initTile ();
				}
				/*
				//移動範囲判定用リストの初期化
				GameManager.Instance.m_CurrentPlayer.initMoveTileList ();
				//分岐点リストの取得
				GetTurningPointList ();
				//分岐番号をセット
				Invoke ("SetMigrationPathwayNumber",1.0f);
				*/
		}

		/// <summary>
		/// 分岐点のリストを取得
		/// </summary>
		private void GetTurningPointList(){
				TurningPointList = new List<TileBase> ();
				for(int i=0;TileContainaer.transform.childCount>i;i++){
						TileBase _tile = TileContainaer.transform.GetChild (i).GetComponent<TileBase> ();
						if (_tile.isTurningPoint&&_tile.isVisible)
								TurningPointList.Add (_tile);
				}
		}
		/// <summary>
		/// タイルに経路番号をセットする
		/// </summary>
		private void SetMigrationPathwayNumber(){
				MigrationPathwayNumber = 0;
				for(int i=0;TurningPointList.Count>i;i++){
						TurningPointList [i].MigrationPathwayNumber = i;
						CurrentMoveTile = TurningPointList [i];
						MigrationPathway = TurningPointList [i];
						SearchMigrationPathway ();
				}
		}
		/// <summary>
		/// 各タイルに経路番号を再帰的にセットする
		/// </summary>
		/// <param name="_MigrationPathwayNumber">Migration pathway number.</param>
		private void SearchMigrationPathway(){

				Debug.Log ("SearchMigrationPathway:"+MigrationPathwayNumber);

				if(CurrentMoveTile == MigrationPathway){//現在位置が初期分岐点の場合
						
						if (CurrentMoveTile.UpTile != null && CurrentMoveTile.UpTile.MigrationPathwayNumber == -1) {
						} else if (CurrentMoveTile.DownTile != null && CurrentMoveTile.DownTile.MigrationPathwayNumber == -1) {
						} else if (CurrentMoveTile.LeftTile != null && CurrentMoveTile.LeftTile.MigrationPathwayNumber == -1) {
						} else if (CurrentMoveTile.RightTile != null && CurrentMoveTile.RightTile.MigrationPathwayNumber == -1) {
						} else {//現在位置が分岐点かつ、全方向に進めない場合
								MigrationPathwayNumber++;
								Debug.Log("return");
								return;
						}
				}

				if (CurrentMoveTile.UpTile != null && CurrentMoveTile.UpTile.MigrationPathwayNumber == -1 && !CurrentMoveTile.UpTile.isTurningPoint) {
						CurrentMoveTile = CurrentMoveTile.UpTile;
						CurrentMoveTile.MigrationPathwayNumber = MigrationPathwayNumber;
						Debug.Log (CurrentMoveTile.name+"="+MigrationPathwayNumber);
				} else if (CurrentMoveTile.DownTile != null && CurrentMoveTile.DownTile.MigrationPathwayNumber == -1 && !CurrentMoveTile.DownTile.isTurningPoint) {
						CurrentMoveTile = CurrentMoveTile.DownTile;
						CurrentMoveTile.MigrationPathwayNumber = MigrationPathwayNumber;
						Debug.Log (CurrentMoveTile.name+"="+MigrationPathwayNumber);
				} else if (CurrentMoveTile.LeftTile != null && CurrentMoveTile.LeftTile.MigrationPathwayNumber == -1 && !CurrentMoveTile.LeftTile.isTurningPoint) {
						CurrentMoveTile = CurrentMoveTile.LeftTile;
						CurrentMoveTile.MigrationPathwayNumber = MigrationPathwayNumber;
						Debug.Log (CurrentMoveTile.name+"="+MigrationPathwayNumber);
				} else if (CurrentMoveTile.RightTile != null && CurrentMoveTile.RightTile.MigrationPathwayNumber == -1 && !CurrentMoveTile.RightTile.isTurningPoint) {
						CurrentMoveTile = CurrentMoveTile.RightTile;
						CurrentMoveTile.MigrationPathwayNumber = MigrationPathwayNumber;
						Debug.Log (CurrentMoveTile.name+"="+MigrationPathwayNumber);
				} else if (CurrentMoveTile.UpTile != null && CurrentMoveTile.UpTile.isTurningPoint) {//分岐点だったときの判定
						Debug.Log("CurrentMoveTile = MigrationPathway");
						MigrationPathwayNumber++;
						CurrentMoveTile = MigrationPathway;
				} else if (CurrentMoveTile.DownTile != null && CurrentMoveTile.DownTile.isTurningPoint) {
						Debug.Log("CurrentMoveTile = MigrationPathway");
						MigrationPathwayNumber++;
						CurrentMoveTile = MigrationPathway;
				} else if (CurrentMoveTile.LeftTile != null && CurrentMoveTile.LeftTile.isTurningPoint) {
						Debug.Log("CurrentMoveTile = MigrationPathway");
						MigrationPathwayNumber++;
						CurrentMoveTile = MigrationPathway;
				} else if (CurrentMoveTile.RightTile != null && CurrentMoveTile.RightTile.isTurningPoint) {
						Debug.Log("CurrentMoveTile = MigrationPathway");
						MigrationPathwayNumber++;
						CurrentMoveTile = MigrationPathway;
				} else {
						
				}

				//再帰的に関数を実行
				SearchMigrationPathway ();


		}

	
}
