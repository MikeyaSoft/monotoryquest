﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Windowの選択肢をPhotonViewを通して同期させる
/// </summary>
public class WindowManager : SingletonMonoBehaviour<WindowManager> {
		[SerializeField]
		private PhotonView m_PhotonView;
		/// <summary>
		/// リストアイテム押下時のコールバック
		/// </summary>
		public Action ListItemCallBack;

		/// <summary>
		/// ポップアップのはいボタン押下時の挙動
		/// </summary>
		public void onClickPopUpYesButton(){
				m_PhotonView.RPC ("onClickPopUpYesButtonRPC", PhotonTargets.All);
		}

		[RPC]
		private void onClickPopUpYesButtonRPC(){
				if (ProductionManager.Instance.m_CurrentPopUp.YesCallBack != null) {
						ProductionManager.Instance.m_CurrentPopUp.YesCallBack ();
						ProductionManager.Instance.m_CurrentPopUp = null;
				}
		}

		/// <summary>
		/// ポップアップのいいえボタン押下時の挙動
		/// </summary>
		public void onClickPopUpNoButton(){
				m_PhotonView.RPC ("onClickPopUpNoButtonRPC", PhotonTargets.All);
		}

		[RPC]
		private void onClickPopUpNoButtonRPC(){

				if (ProductionManager.Instance.m_CurrentPopUp.NoCallBack != null) {
						ProductionManager.Instance.m_CurrentPopUp.HideAndTurnEnd ();
						ProductionManager.Instance.m_CurrentPopUp = null;
				}
		}


		/// <summary>
		/// カード使用windowの表示
		/// </summary>
		public void onClickCardListButton(){
				m_PhotonView.RPC ("onClickCardListButtonRPC", PhotonTargets.All);
		}
		[RPC]
		private void onClickCardListButtonRPC(){
				ProductionManager.Instance.AppendScrollListWindow ("カード一覧",GameManager.Instance.AppendCardListItem);
				ProductionManager.Instance.m_CurrentListWindow.SetSubWindowText ("使いたいカードを選択してね");
		}


		/// <summary>
		/// リストウィンドウのいいえボタン押下時の挙動
		/// </summary>
		public void onClickListWindowNoButton(){
				m_PhotonView.RPC ("onClickListWindowNoButtonRPC", PhotonTargets.All);
		}
		[RPC]
		private void onClickListWindowNoButtonRPC(){
				ProductionManager.Instance.m_CurrentListWindow.HideAndTurnEnd ();
		}


		//----------


		/// <summary>
		/// リストアイテムボタン押下時の挙動
		/// </summary>
		public void onClickListItemButton(){
				m_PhotonView.RPC ("onClickListItemButtonRPC", PhotonTargets.All);
		}
		[RPC]
		private void onClickListItemButtonRPC(){
				if (ListItemCallBack != null) {
						ListItemCallBack ();
				} else {
						Debug.Log ("onClickListItemButton");
				}
		}


	
}
