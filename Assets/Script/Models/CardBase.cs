﻿/// <summary>
/// カードのベースモデルクラス
/// </summary>
public class CardBase {
		/// <summary>
		/// CardId
		/// </summary>
		public int CardId;
		/// <summary>
		/// カードの名前
		/// </summary>
		public string CardName;
		/// <summary>
		/// カードの説明
		/// </summary>
		public string CardNote;
		/// <summary>
		/// カードの価格
		/// </summary>
		public int CardPrice;
		/// <summary>
		/// カードの属性：0:妨害、1:回復、2:進行
		/// </summary>
		public int Attribute;
		/// <summary>
		/// カードのレアリティ：0~5
		/// </summary>
		public int Rarity;

		public CardBase(int _CardId,string _CardName,string _CardNote,int _CardPrice,int _Attribute,int _Rarity){
				CardId = _CardId;
				CardName = _CardName;
				CardNote = _CardNote;
				CardPrice = _CardPrice;
				Attribute = _Attribute;
				Rarity = _Rarity;
		}
		//クラスからデータを複製する場合
		public CardBase(CardBase _CardBase){
				CardId = _CardBase.CardId;
				CardName = _CardBase.CardName;
				CardNote = _CardBase.CardNote;
				CardPrice = _CardBase.CardPrice;
				Attribute = _CardBase.Attribute;
				Rarity = _CardBase.Rarity;
		}
}
