﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Player : MonoBehaviour {
		RaycastHit hit;
	
	//-------------------同期するパラメーター
	
		//移動距離を保持するメンバ変数
		public int MoveCount = 4;

	//-------------------同期しないパラメーター

		/// <summary>
		/// 現在止まっているタイル情報をキャッシュする
		/// </summary>
		private TileBase CurrentMoveTile;
		/// <summary>
		/// 移動判定中か否か
		/// </summary>
		private bool isMoveRenge;
		/// <summary>
		/// 移動できる範囲選択時に移動したタイルを一時的に格納
		/// </summary>
		public List<TileBase> MoveTileList;
		/// <summary>
		/// 移動可能タイルのリスト：TileStatsが０のものを格納
		/// </summary>
		public List<TileBase> RemovableAreaTileList;
		/// <summary>
		/// 移動判定時か否か
		/// </summary>
		private bool isMoveTileList;
		/// <summary>
		/// 所持金
		/// </summary>
		public int Money;
		/// <summary>
		/// 所持物件リスト
		/// </summary>
		public List<TileBase> ArticleList;
		/// <summary>
		/// 経路選択リスト
		/// </summary>
		public List<TileBase> MigrationPathwayList;
		/// <summary>
		/// 移動経路選択初期フラグ
		/// </summary>
		private bool isFastMigrationPathway;
		/// <summary>
		/// トゥウィーンアニメ用
		/// </summary>
		private TweenAnimetionGlobal m_TweenAnime;
		/// <summary>
		/// プレイヤーのZ座標
		/// </summary>
		private const float PLAYER_Z_POSITION = -146.0f;
		/// <summary>
		/// プレイヤーの所持カード
		/// </summary>
		public List<CardBase> m_CardList;
		/// <summary>
		/// 通過したマークの情報を保持
		/// </summary>
		public List<int> MarkList;


		/// <summary>
		/// 周回数
		/// </summary>
		public int NumberOfGoing;
		/// <summary>
		/// 現在プレイヤーのカーソルを表示
		/// </summary>
		public GameObject m_Curser;
		/// <summary>
		/// キャラクターのスプライト画像
		/// </summary>
		public UISprite m_CharaSprite;
		/// <summary>
		/// 移動アニメ完了後のコールバック
		/// </summary>
		public Action MoveAnimeCallBack;
		/// <summary>
		/// 移動アニメ完了後のコールバック後のアクション(コンピューターAIのみ)
		/// </summary>
		public Action TileEventAI;
		/// <summary>
		/// プレイヤーの総資産
		/// </summary>
		public int TotalAssets;
		public bool isPlayer=false;
		public bool isComputer=false;
		/// <summary>
		/// 目的地までの経路リスト
		/// </summary>
		public List<TileBase> DestinationList;
		/// <summary>
		/// 前回移動時にキャッシュした下部のタイル情報をキャッシュ
		/// </summary>
		private TileBase PrevUnderTile;
		/// <summary>
		/// プレイヤー番号
		/// </summary>
		public int PlayerNumber;
		/// <summary>
		/// UserName
		/// </summary>
		public string UserName="てすた";
		/// <summary>
		/// MoveCountの値をコピー
		/// </summary>
		private int CopyMoveCount;
		private TileBase UnderTile;
		public PhotonView m_PhotonView;
		/// <summary>
		/// カード取得時の乱数を保持する
		/// </summary>
		public int RandomCardNumber;
		public BaseAI ai;

		[SerializeField]
		private GameObject m_PlayerStatus;
		/*
		[SerializeField]
		private UIGrid m_PlayerStatusContainer;
		*/

		void Awake(){
				//取り急ぎPrototypeAIを使う
				ai = new PrototypeAI ();
				UnderTile = TileManager.Instance.m_BankTile;
				//取り急ぎ1000Gに設定
				Money = StaticData.EARLY_MONEY;
				RandomCardNumber = 0;
				TotalAssets = StaticData.EARLY_MONEY;
				m_CardList = new List<CardBase> ();
				ArticleList = new List<TileBase>();
				DestinationList = new List<TileBase> ();
				m_CharaSprite = this.transform.FindChild ("UISprite").GetComponent<UISprite> ();
				m_Curser.SetActive (false);
				NumberOfGoing = 1;
				m_PhotonView = this.transform.GetComponent<PhotonView> ();
				MarkList = new List<int> ();
				m_TweenAnime = this.GetComponent<TweenAnimetionGlobal> ();

				//-----photon
				if (!isComputer) {//コンピューターでないとき
						GameManager.Instance.m_Players.Add (this);
						this.transform.parent = GameManager.Instance.m_PlayerContainer.transform;
						this.transform.localScale = Vector3.one;

						//----ステータスウィンドのInstantiate
						GameObject _window = Instantiate (m_PlayerStatus) as GameObject;
						PlayerStatusWindowCotroller _cont = _window.GetComponent<PlayerStatusWindowCotroller> ();
						GameManager.Instance.PlayerStatusWindowCotrollerList.Add (_cont);
						_cont.m_Player = this;
						_window.transform.parent = GameManager.Instance.m_PlayerStatusContainer.transform;
						_window.transform.localScale = Vector3.one;
						this.UserName = PhotonNetwork.player.name;
						//同期処理等
						this.name = "player_" + (m_PhotonView.ownerId) + "_" + PhotonNetwork.player.ID + "_" + PhotonNetwork.player.name;
						PlayerNumber = m_PhotonView.ownerId;
						//プレイヤーのイメージをセット
						switch (m_PhotonView.ownerId) {
						case 1:
								SetPlayerSprite ("sakuya");
								break;
						case 2:
								SetPlayerSprite ("flan");
								break;
						}
				}

		}

		/// <summary>
		/// プレイヤーのイメージをセット
		/// </summary>
		private void SetPlayerSprite(string _spritename){
				m_CharaSprite.spriteName = _spritename+"_0";
				m_CharaSprite.GetComponent<UISpriteAnimation> ().namePrefix = _spritename;
				
		}

		void Start(){
				isMoveTileList = false;
				UnderTile = TileManager.Instance.m_BankTile;

				//Invoke ("CalculationRemovableArea",0.4f);

		}


		//データの同期処理
		void OnPhotonSerializeView (PhotonStream stream, PhotonMessageInfo info)
		{
				if (isComputer)
						return;//コンピューターの時は処理を飛ばす


				if (stream.isWriting) {//データの送信(自分のパラメーターを送信する)

						if (GameManager.Instance.isLeadyPlayerSync) {
								SendParam(stream);

						}

				} else if(stream.isReading) {//データの受信(相手のパラメーターを受信する)

						if (GameManager.Instance.isLeadyPlayerSync) {

								ReceiveParam(stream);

						}
				}



		}

		//データの送信(自分のパラメーターを送信する)
		private void SendParam(PhotonStream stream){
				stream.SendNext (MoveCount);
				stream.SendNext (UserName);
		}
		//データの受信(相手のパラメーターを受信する)
		private void ReceiveParam(PhotonStream stream){
				MoveCount = (int)stream.ReceiveNext ();
				UserName = (string)stream.ReceiveNext ();
		}

		//----------------

		public void SetRandomCardNumber(int RandomNumebr){
				string s_RandomNumebr = "" + RandomNumebr;
				m_PhotonView.RPC ("RandomCardNumberRPC", PhotonTargets.All, s_RandomNumebr);
		}

		[RPC]
		public void RandomCardNumberRPC(string RandomNumebr){
				RandomCardNumber = int.Parse (RandomNumebr);
		}


		//-----------------

		public void MoveAnimeRPC(List<TileBase> _tileList){
				Debug.Log ("MoveAnimeRPC");
				string _data = "";

				for (int i = 0; _tileList.Count > i; i++) {
						_data += _tileList[i].TileID+ "/";
				}

				m_PhotonView.RPC ("MoveAnime", PhotonTargets.All, _data);
		}

		[RPC]
		public void MoveAnime(string _data){
				Debug.Log ("MoveAnime");
				string[] dataArry = _data.Split ('/');
				List<TileBase> _tileList = new List<TileBase> ();
				//listの復元
				for(int i=0;dataArry.Length>i;i++){
						if (dataArry [i] != "") {//空文字でないなら
								_tileList.Add (TileManager.Instance.GetTileBaseFromID (int.Parse (dataArry [i])));
						}
				}
				StartCoroutine (PlayMoveAnime(_tileList));
		}

		private IEnumerator PlayMoveAnime(List<TileBase> _tileList){
				Debug.Log ("PlayMoveAnime");
				for(int i=0;_tileList.Count>i;i++){


						if (_tileList [i].KindOfTile >= 4 && _tileList [i].KindOfTile <= 7) {//マーク通過
								if (!MarkList.Contains (_tileList [i].KindOfTile)) {
										ProductionManager.Instance.AppendMarkFx(_tileList [i].KindOfTile,_tileList [i].gameObject);
										int MarkKind = _tileList [i].KindOfTile;
										MarkList.Add (MarkKind);
										//目的地リストの初期化
										DestinationList = new List<TileBase> ();
										Debug.Log (this.gameObject.name+":MarkList.Add"+_tileList [i].KindOfTile);

								}
						}else if(_tileList [i].KindOfTile == 3 && i != _tileList.Count){//銀行通過時のみ実行
								//銀行イベント
								EventManager.Instance.BankEvent(this,1.0f);
						}



						m_TweenAnime.playAnimation (_tileList[i].gameObject);
						if (i == _tileList.Count-1) {
								m_TweenAnime.CallBack = ResetPosition;
						}
						AudioManager.Instance.PlaySE (7);
						yield return new WaitForSeconds(m_TweenAnime.animeTime);
				}

				GameManager.Instance.m_CurrentTile = _tileList [_tileList.Count-1];
				PrevUnderTile = _tileList[_tileList.Count-1];

				//移動アニメ実行後にタイルの種類に応じたイベントを実行する
				GameManager.Instance.m_CurrentPlayer.MoveAnimeCallBack = GameManager.Instance.m_CurrentTile.TileEvent;

				//ステータスをシンクロさせる
				GameManager.Instance.SyncPlayerStatus ();
				yield return new WaitForSeconds(0.4f);
				//画面のセンタリング
				GameManager.Instance.CenteringScroll ();
				yield return new WaitForSeconds(0.4f);
				//アニメ終了後にコールバックを呼び出す
				if (MoveAnimeCallBack != null) {
						//windowなどの表示
						MoveAnimeCallBack ();
						if (TileEventAI != null) {
								yield return new WaitForSeconds(0.4f);
								//コンピューターの思考AI
								TileEventAI ();
						}
				}
				//タイルの初期化
				TileManager.Instance.initTiles ();
		}


		public void ResetPosition(){
				Debug.Log ("ResetPosition");
				this.transform.localPosition = new Vector3 (this.transform.localPosition.x,this.transform.localPosition.y,PLAYER_Z_POSITION);
		}


		/// <summary>
		/// 移動可能範囲のタイルを取得
		/// </summary>
		private void GetRemovableAreaTileList(){
				RemovableAreaTileList = new List<TileBase> ();
				for(int i=0;MoveTileList.Count>i;i++){
						if (MoveTileList [i].TileState == 0) {
								RemovableAreaTileList.Add (MoveTileList [i]);
								//選択可能にする
								MoveTileList [i].isPossibleToChoose = true;
						}
						
				}
		}

		/// <summary>
		/// プレイヤーオブジェクトの座標を指定箇所に移動させる
		/// </summary>
		public void MovePlayer(Transform _target){
				this.transform.position = new Vector3 (_target.position.x,_target.position.y,this.transform.position.z);

		}

		/// <summary>
		/// サイコロを振る関数
		/// </summary>
		public void SetDiceNumber(){
				MoveCount = UnityEngine.Random.Range (1, 7);
		}

		public void initMoveTileList(){
				MoveTileList = new List<TileBase> ();
				MigrationPathwayList = new List<TileBase> ();
		}

		/// <summary>
		/// 移動範囲の割り出し
		/// </summary>
		public void CalculationRemovableArea(){
				initPlayer ();
				initMoveTileList();
				isFastMigrationPathway = true;
				ExcuteSingleSetMoveRenge ();
				isFastMigrationPathway = false;
				SetMoveRengeFromMoveTileList ();
				SetMoveRengeFromMoveTileList ();
				SetMoveRengeFromMoveTileList ();
				GetRemovableAreaTileList ();
		}


		private void initPlayer(){
				//プレイヤー直下のタイルを取得
				GetUnderTileInfo ();
				CopyMoveCount = MoveCount;
		}

		/// <summary>
		/// 指定方向に行けるか否か調べる関数
		/// </summary>
		private bool SearchMove(Vector3 Vector){

				if (Vector == Vector3.up && CurrentMoveTile.UpTile != null && CurrentMoveTile.UpTile.TileState == -1) {

						SetCurrentTileState (CurrentMoveTile.UpTile);
						return true;
				} else if (Vector == Vector3.down && CurrentMoveTile.DownTile != null && CurrentMoveTile.DownTile.TileState == -1) {
						SetCurrentTileState (CurrentMoveTile.DownTile);
						return true;
				} else if (Vector == Vector3.left && CurrentMoveTile.LeftTile != null && CurrentMoveTile.LeftTile.TileState == -1) {
						SetCurrentTileState (CurrentMoveTile.LeftTile);
						return true;
				} else if (Vector == Vector3.right && CurrentMoveTile.RightTile != null && CurrentMoveTile.RightTile.TileState == -1) {
						SetCurrentTileState (CurrentMoveTile.RightTile);
						return true;
				} else {
						return false;
				}

		}

		private void SetCurrentTileState(TileBase _CurrentMoveTile){
				CopyMoveCount--;
				CurrentMoveTile = _CurrentMoveTile;


				//まだ追加されていない要素だけを追加する
				if (!MoveTileList.Contains(CurrentMoveTile))
				{
						MoveTileList.Add (CurrentMoveTile);

				}


				if (CopyMoveCount >= 0) {
						CurrentMoveTile.TileState = CopyMoveCount;
						/*経路取得アルゴリズム*/
						MigrationPathwayList.Add (CurrentMoveTile);
						CurrentMoveTile.MigrationPathwayTileList = new List<TileBase> (MigrationPathwayList);
						/*経路取得アルゴリズム*/
				}
				if(!isComputer){
						CurrentMoveTile.SetBgColor (CopyMoveCount);
				}

		}

		/// <summary>
		/// MoveTileList1から移動範囲を割り出す
		/// </summary>
		private void SetMoveRengeFromMoveTileList(){
				for(int i=0;MoveTileList.Count>i;i++){
						CopyMoveCount = MoveTileList [i].TileState;
						CurrentMoveTile = MoveTileList [i];
						if(MoveTileList [i].MigrationPathwayTileList != null)
						MigrationPathwayList = new List<TileBase> (MoveTileList [i].MigrationPathwayTileList);
						ExcuteSingleSetMoveRenge ();
				}
				for(int i=0;MoveTileList.Count>i;i++){
						CopyMoveCount = MoveTileList [i].TileState;
						CurrentMoveTile = MoveTileList [i];
						if(MoveTileList [i].MigrationPathwayTileList != null)
						MigrationPathwayList = new List<TileBase> (MoveTileList [i].MigrationPathwayTileList);
						ExcuteSingleSetMoveRengeDown ();
				}
				for(int i=0;MoveTileList.Count>i;i++){
						CopyMoveCount = MoveTileList [i].TileState;
						CurrentMoveTile = MoveTileList [i];
						if(MoveTileList [i].MigrationPathwayTileList != null)
						MigrationPathwayList = new List<TileBase> (MoveTileList [i].MigrationPathwayTileList);
						ExcuteSingleSetMoveRengeLeft ();
				}
				for(int i=0;MoveTileList.Count>i;i++){
						CopyMoveCount = MoveTileList [i].TileState;
						CurrentMoveTile = MoveTileList [i];
						if(MoveTileList [i].MigrationPathwayTileList != null)
						MigrationPathwayList = new List<TileBase> (MoveTileList [i].MigrationPathwayTileList);
						ExcuteSingleSetMoveRengeRight ();
				}

		}




		/// <summary>
		/// 四方向にSingleSetMoveRengeを繰り出す(上から判定)
		/// </summary>
		private void ExcuteSingleSetMoveRenge(){
				if(isFastMigrationPathway)//移動経路取得が初期なら
					MigrationPathwayList = new List<TileBase> ();
				SingleSetMoveRenge(Vector3.up);
				if(isFastMigrationPathway)//移動経路取得が初期なら
					MigrationPathwayList = new List<TileBase> ();
				SingleSetMoveRenge(Vector3.down);
				if(isFastMigrationPathway)//移動経路取得が初期なら
					MigrationPathwayList = new List<TileBase> ();
				SingleSetMoveRenge(Vector3.right);
				if(isFastMigrationPathway)//移動経路取得が初期なら
					MigrationPathwayList = new List<TileBase> ();
				SingleSetMoveRenge(Vector3.left);
		}
		/// <summary>
		/// 四方向にSingleSetMoveRengeを繰り出す(下から判定する版)
		/// </summary>
		private void ExcuteSingleSetMoveRengeDown(){
				SingleSetMoveRenge(Vector3.down);
		}
		/// <summary>
		/// 四方向にSingleSetMoveRengeを繰り出す(左から判定する版)
		/// </summary>
		private void ExcuteSingleSetMoveRengeLeft(){
				SingleSetMoveRenge(Vector3.left);
		}
		/// <summary>
		/// 四方向にSingleSetMoveRengeを繰り出す(右から判定する版)
		/// </summary>
		private void ExcuteSingleSetMoveRengeRight(){
				SingleSetMoveRenge(Vector3.right);
		}

		/// <summary>
		/// 単一移動可能範囲を割り出す
		/// </summary>
		private void SingleSetMoveRenge(Vector3 _Vector){

				if(UnderTile != null)
					UnderTile.TileState = 0;

				if (SearchMove (_Vector)&&CopyMoveCount > 0) {
				} else if (CurrentMoveTile != UnderTile) {
						//移動範囲割り出しの初期化

						CurrentMoveTile = UnderTile;
						CopyMoveCount = MoveCount;
				} else {
						return;
				}

				//再起的に関数を呼び出す
				if (CopyMoveCount >= 0) {
						SingleSetMoveRenge (_Vector);
				}
		}


		/// <summary>
		/// プレイヤー直下のタイル情報を取得する
		/// </summary>
		public void GetUnderTileInfo(){
				//Vector3.left:左
				//Vector3.down:下
				//Vector3.up:上
				//Vector3.right:右
				//Vector3.forward:直下
				if (Physics.Raycast(transform.position, Vector3.forward, out hit, 10000))
				{
						UnderTile = hit.transform.GetComponent<TileBase> ();

						CurrentMoveTile = UnderTile;
				}
				else
				{
						Debug.LogError ("UnderTile=null");
						UnderTile = PrevUnderTile;
				}
		}
}
