﻿public static class StaticData{
		/// <summary>
		/// マークのスプライト名
		/// </summary>
		public enum MarkName
		{
				Tile_Crow,
				Tile_Dia,
				Tile_Hart,
				Tile_Spade,
		}
		/// <summary>
		/// ゲーム開始時の初期金額
		/// </summary>
		public const int EARLY_MONEY = 4900;
		/// <summary>
		/// カード所持の上限数
		/// </summary>
		public const int MAX_CARD_NUM = 5;
}
