﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class TileBase : MonoBehaviour {
	
		private RaycastHit hit;
		public int TileState;
		/// <summary>
		/// タイルの識別ID
		/// </summary>
		public int TileID;
		public TileBase UpTile;
		public TileBase DownTile;
		public TileBase LeftTile;
		public TileBase RightTile;
		/// <summary>
		/// 隣接タイル数
		/// </summary>
		public int NumberOfNextTiles;
		/// <summary>
		/// 見えるか否か
		/// </summary>
		public bool isVisible = true;
		/// <summary>
		/// 背景のスプライト
		/// </summary>
		public UISprite m_BG;
		/// <summary>
		/// タイルの種類
		/// </summary>
		public int KindOfTile=0;
		/// <summary>
		/// 選択可能か否か
		/// </summary>
		public bool isPossibleToChoose;
		/// <summary>
		/// 所持者番号
		/// </summary>
		public int HolderNumber=0;
		/// <summary>
		/// 物件価格
		/// </summary>
		public int PropertyPrice=0;
		/// <summary>
		/// 通行量
		/// </summary>
		public int PropertyTax = 0;
		/// <summary>
		/// 物件のランク：0~2;
		/// </summary>
		public int PropertyRanck = 0;
		/// <summary>
		/// 通行料
		/// </summary>
		public int AmountOfTraffic=0;
		/// <summary>
		/// 物件名
		/// </summary>
		public string PropertyName="てすと物件";

		/// <summary>
		/// 表示オブジェクトのコンテイナー
		/// </summary>
		[SerializeField]
		private GameObject m_Container;
		/// <summary>
		/// タイルの画像
		/// </summary>
		[SerializeField]
		public UISprite BackGround;
		/// <summary>
		/// 物件価格を表示
		/// </summary>
		[SerializeField]
		public UILabel MoneyLabel;
		/// <summary>
		/// 通行料を表示
		/// </summary>
		[SerializeField]
		public UILabel TaxLabel;
		/// <summary>
		/// 移動可能範囲を表示
		/// </summary>
		[SerializeField]
		public GameObject MoveTile;
		/// <summary>
		/// 移動経路格納リスト
		/// </summary>
		public List<TileBase> MigrationPathwayTileList;
		/// <summary>
		/// 分岐点か否か
		/// </summary>
		public bool isTurningPoint;
		/// <summary>
		/// 属している経路番号
		/// </summary>
		public int MigrationPathwayNumber;


		void Awake(){
				isTurningPoint = false;
				MigrationPathwayNumber = -1;
				
		}

		/// <summary>
		/// tileの状態を初期化
		/// </summary>
		public void initTile(){
				MoveTile.SetActive (false);
				NumberOfNextTiles = 0;
				TileState = -1;
				isPossibleToChoose = false;
				m_Container.SetActive (isVisible);
				GetTileInfo ();
				initBgColor ();
				MigrationPathwayTileList = null;
		}

		/// <summary>
		/// タイル押下時の挙動
		/// </summary>
		public void onClickTile(){
				if (!isPossibleToChoose)
						return;
				Debug.Log ("onClickTile");

				//移動アニメ実行後にタイルの種類に応じたイベントを実行する
				if (GameManager.Instance.m_CurrentPlayer.isComputer) {
						//移動アニメ完了後のコンピューターアクション
						GameManager.Instance.m_CurrentPlayer.TileEventAI = AIManager.Instance.TileEventAI;
				}

				int CardRandNumber = UnityEngine.Random.Range (0, DataManager.Instance.CardData.Count);

				GameManager.Instance.m_CurrentPlayer.SetRandomCardNumber (CardRandNumber);

				GameManager.Instance.m_CurrentPlayer.MoveAnimeRPC (MigrationPathwayTileList);



		}

		public void TileEvent(){
				//タイルの種類に応じてイベントを発生させる
				EventManager.Instance.ExcuteEvent (KindOfTile);
		}

		/// <summary>
		/// 物件購入時に購入者に応じて物件の色を変える
		/// </summary>
		public void SetTileBg(int PlayerNumber){
				switch(PlayerNumber){
				case 1:
						BackGround.spriteName = "Tile_blue";
						break;
				case 2:
						BackGround.spriteName = "Tile_green";
						break;
				case 3:
						BackGround.spriteName = "Tile_pink";
						break;
				case 4:
						BackGround.spriteName = "Tile_yellow";
						break;
				}
		}

		private void initBgColor(){
				MoveTile.SetActive (false);
		}
		public void SetBgColor(int _TileState){
				if (_TileState == 0) {
						MoveTile.SetActive (true);
				}
		}
	
		private void GetTileInfo(){
				//ゲームの開始時のみ実行する
				if (!TileManager.Instance.isFast)
						return;
				//Vector3.left:左
				//Vector3.down:下
				//Vector3.up:上
				//Vector3.right:右
				//Vector3.forward:直下
				if (Physics.Raycast(transform.position, Vector3.up, out hit, 1))
				{
						
						if (hit.transform.GetComponent<TileBase> ().isVisible) {
								UpTile = hit.transform.GetComponent<TileBase> ();
								NumberOfNextTiles++;
						}
				}
				if (Physics.Raycast(transform.position, Vector3.down, out hit, 1))
				{
						
						if (hit.transform.GetComponent<TileBase> ().isVisible) {
								DownTile = hit.transform.GetComponent<TileBase> ();
								NumberOfNextTiles++;
						}
				}
				if (Physics.Raycast(transform.position, Vector3.left, out hit, 1))
				{
						
						if (hit.transform.GetComponent<TileBase> ().isVisible) {
								LeftTile = hit.transform.GetComponent<TileBase> ();
								NumberOfNextTiles++;
						}
				}
				if (Physics.Raycast(transform.position, Vector3.right, out hit, 1))
				{
						
						if (hit.transform.GetComponent<TileBase> ().isVisible) {
								RightTile = hit.transform.GetComponent<TileBase> ();
								NumberOfNextTiles++;
						}
				}

				if (NumberOfNextTiles >= 3) {//隣接タイルが三つ以上のときは分岐点になる
						isTurningPoint = true;
				}

		}
	
}
