﻿using UnityEngine;
using System.Collections;
using ExitGames.Client.Photon;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using System;
using UniRx;
using UniRx.Triggers;

public class LobbySceneController : SingletonMonoBehaviour<LobbySceneController>, IPunCallbacks
{
	[SerializeField]
	private GameObject m_RoomButton;
	[SerializeField]
	private UIGrid m_UIGrid;
	[SerializeField]
	public UILabel m_InPutLabel;
	[SerializeField]
	private UIButton m_CreateRoomButton;
	private int PrevRoomInfoCount;
	private int RoomInfoCount;
	[SerializeField]
	private GameObject mLoadScreen;
	[SerializeField]
	UILabel mNoRoomlabel;

	// Use this for initialization
	void Start ()
	{
			PrevRoomInfoCount = 0;
			RoomInfoCount = 0;
			//一旦オブジェクトを破棄する
			foreach (Transform n in m_UIGrid.transform) {
					GameObject.Destroy (n.gameObject);
			}
	}

	private void CreateRoomButton (RoomInfo room)
	{
	
			GameObject _RoomButton = Instantiate (m_RoomButton) as GameObject;
			if (!room.open) {
					_RoomButton.GetComponent<UIButton> ().isEnabled = false;
			}
			_RoomButton.name = room.name;
			_RoomButton.transform.FindChild ("Label").GetComponent<UILabel> ().text = room.customProperties ["LocalName"].ToString ();
			_RoomButton.transform.parent = m_UIGrid.transform;
			_RoomButton.transform.localScale = Vector3.one;
			_RoomButton.transform.localPosition = Vector3.zero;
	}

	/// <summary>
	/// 部屋を作成後に次画面へ進む
	/// </summary>
	public void CreateRoom ()
	{
		Guid guidValue = Guid.NewGuid ();
		PhotonNetwork.CreateRoom ("部屋" + guidValue.ToString (), createRoomOptions (), TypedLobby.Default);
		PhotonNetwork.player.name = GetInPutLabelText ();
		this.gameObject.SetActive (false);
		/*
		mLoadScreen.SetActive (true);
		var disposableGameObject = new GameObject ();
		this.UpdateAsObservable ().Subscribe ((_) => {
			if (PhotonNetwork.inRoom){
				mLoadScreen.SetActive (false);
				Destroy(disposableGameObject);
			}
		}).AddTo(disposableGameObject);
		*/
		//PhotonNetwork.JoinRoom(m_InPutLabel.text);
		//Application.LoadLevel("Game");
	}

	public string GetInPutLabelText ()
	{
			string name = m_InPutLabel.text;
			if (name == "プレイヤー名を入力") {
					name = "てすた";
			}
			return name;
	}

	private RoomOptions createRoomOptions ()
	{
			RoomOptions options = new RoomOptions ();
			options.isOpen = true;
			options.isVisible = true;
			options.maxPlayers = 4;

			options.customRoomProperties = new Hashtable {
					{ "LocalName", "部屋" + RoomInfoCount },
					{ "RoomState", "Idling" },
					{ "TestPalam2", 1 },
			};
			options.customRoomPropertiesForLobby = new string[] { "LocalName" };

			return options;
	}

	private void ReConstructRoomButtons ()
	{
			RoomInfo[] _RoomInfoArry = PhotonNetwork.GetRoomList ();
			RoomInfoCount = _RoomInfoArry.Length;
			//一旦オブジェクトを破棄する
			foreach (Transform n in m_UIGrid.transform) {
					GameObject.Destroy (n.gameObject);
			}
			for (int i = 0; RoomInfoCount > i; i++) {
					if (_RoomInfoArry [i].visible) {
							CreateRoomButton (_RoomInfoArry [i]);
					}
			}

	}

	// Update is called once per frame
	void Update ()
	{
			if (PhotonNetwork.connecting) {

			} else if (!PhotonNetwork.connected) {

					PhotonNetwork.ConnectUsingSettings ("0.1");

			} else {

					m_CreateRoomButton.gameObject.SetActive (true);

					RoomInfo[] _RoomInfoArry = PhotonNetwork.GetRoomList ();
					RoomInfoCount = _RoomInfoArry.Length;
					if(_RoomInfoArry.Length != 0){
						mNoRoomlabel.gameObject.SetActive(false);
					}else{
						mNoRoomlabel.gameObject.SetActive(true);
					}
					/**/
					if (RoomInfoCount == PrevRoomInfoCount)
							return;
					ReConstructRoomButtons ();
					PrevRoomInfoCount = PhotonNetwork.GetRoomList ().Length;

			}

	}

	#region IPunCallbacks

	public void OnConnectedToPhoton ()
	{
			Debug.Log ("OnConnectedToPhoton");
	}

	public void OnLeftRoom ()
	{
			Debug.Log ("OnLeftRoom");
	}

	public void OnMasterClientSwitched (PhotonPlayer newMasterClient)
	{
			Debug.Log ("OnMasterClientSwitched");
	}

	public void OnPhotonCreateRoomFailed (object[] codeAndMsg)
	{
			Debug.Log ("OnPhotonCreateRoomFailed");
	}

	public void OnPhotonJoinRoomFailed (object[] codeAndMsg)
	{
			Debug.Log ("OnPhotonJoinRoomFailed");
	}

	public void OnCreatedRoom ()
	{
			Debug.Log ("OnCreatedRoom");
	}

	public void OnJoinedLobby ()
	{
			Debug.Log ("OnJoinedLobby");
	}

	public void OnLeftLobby ()
	{
			Debug.Log ("OnLeftLobby");
	}

	public void OnFailedToConnectToPhoton (DisconnectCause cause)
	{
			Debug.Log ("OnFailedToConnectToPhoton");
	}

	public void OnConnectionFail (DisconnectCause cause)
	{
			Debug.Log ("OnConnectionFail");
	}

	public void OnDisconnectedFromPhoton ()
	{
			Debug.Log ("OnDisconnectedFromPhoton");
	}

	public void OnPhotonInstantiate (PhotonMessageInfo info)
	{
			Debug.Log ("OnPhotonInstantiate");
	}
	//RoomListにUPDateが入ったとき
	public void OnReceivedRoomListUpdate ()
	{
			ReConstructRoomButtons ();

			PrevRoomInfoCount = PhotonNetwork.GetRoomList ().Length;
			Debug.Log ("OnReceivedRoomListUpdate");
	}

	public void OnJoinedRoom ()
	{
			Debug.Log ("OnJoinedRoom");
	}

	public void OnPhotonPlayerConnected (PhotonPlayer newPlayer)
	{
			Debug.Log ("OnPhotonPlayerConnected");
	}

	public void OnPhotonPlayerDisconnected (PhotonPlayer otherPlayer)
	{
			Debug.Log ("OnPhotonPlayerDisconnected");
	}

	public void OnPhotonRandomJoinFailed (object[] codeAndMsg)
	{
			Debug.Log ("OnPhotonRandomJoinFailed");
	}

	public void OnConnectedToMaster ()
	{
			Debug.Log ("OnConnectedToMaster");
	}

	public void OnPhotonMaxCccuReached ()
	{
			Debug.Log ("OnPhotonMaxCccuReached");
	}

	public void OnPhotonCustomRoomPropertiesChanged (Hashtable propertiesThatChanged)
	{
			Debug.Log ("OnPhotonCustomRoomPropertiesChanged");
	}

	public void OnPhotonPlayerPropertiesChanged (object[] playerAndUpdatedProps)
	{
			Debug.Log ("OnPhotonPlayerPropertiesChanged");
	}

	public void OnUpdatedFriendList ()
	{
			Debug.Log ("OnUpdatedFriendList");
	}

	public void OnCustomAuthenticationFailed (string debugMessage)
	{
			Debug.Log ("OnCustomAuthenticationFailed");
	}

	public void OnWebRpcResponse (OperationResponse response)
	{
			Debug.Log ("OnWebRpcResponse");
	}

	public void OnOwnershipRequest (object[] viewAndPlayer)
	{
			Debug.Log ("OnOwnershipRequest");
	}

	#endregion
}
