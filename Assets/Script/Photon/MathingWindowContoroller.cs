﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MathingWindowContoroller : MonoBehaviour
{

	[SerializeField]
	private UILabel m_StatuLabel;
	[SerializeField]
	private GameObject m_LoadingShield;
	[SerializeField]
	private PhotonView m_PhotonView;


	private int WaitCount;




	void Awake(){
			WaitCount = 0;
	}
	// Update is called once per frame
	void Update ()
	{
			if (PhotonNetwork.inRoom) {
					m_StatuLabel.text = "現在の人数:"+PhotonNetwork.room.playerCount+"人";
					m_LoadingShield.SetActive (false);
			} else {
					if (WaitCount >= 1200) {
							PhotonManager.Instance.GameReset ();
					}
					m_LoadingShield.SetActive (true);
					WaitCount++;
			}
	}

	[ContextMenu("ゲームスタートテスト")]
	public void GameStartRPC(){
			Debug.Log ("GameStartRPC");
			m_PhotonView.RPC ("GameStart", PhotonTargets.All);
	}

	[RPC]
	public void GameStart(){
			Debug.Log ("GameStart");
			GameManager.Instance.StartGameRPC ();
	}
}
