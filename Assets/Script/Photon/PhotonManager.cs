﻿using UnityEngine;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using UniRx;

public class PhotonManager : Photon.MonoBehaviour
{
		/// <summary>
		/// 読み込むprefab名
		/// </summary>
		public string objectName;
		/// <summary>
		/// 生成したプレイヤーデータを格納する
		/// </summary>
		//public List<Player> m_PhotonPlayerList;
		/// <summary>
		/// シングルトン化
		/// </summary>
		private static PhotonManager mInstance;
		public static PhotonManager Instance {
				get {
						if (mInstance == null)
								mInstance = new PhotonManager ();
						return mInstance;
				}
		}


		public int RoomJoinCount;
		[SerializeField]
		private PhotonView m_PhotonView;








		void Awake ()
		{

				mInstance = this;
				// 自動スリープを無効にする
				Screen.sleepTimeout = SleepTimeout.NeverSleep;

		}

		void Start ()
		{
				// Photonへの接続を行う
				PhotonNetwork.ConnectUsingSettings ("0.1");
				// 同期するタイミングを変更します
				PhotonNetwork.sendRate = 60;
				RoomJoinCount = 0;
				//リストの初期化
				//m_PhotonPlayerList = new List<Player> ();


		}

		/// <summary>
		/// ルームに入室成功した場合、コールバックされるメソッド
		/// </summary>
		void OnJoinedRoom ()
		{
				Debug.Log ("OnJoinedRoom");
				RoomJoinCount++;
				addNetworkPlayer ();

		}

		//ゲームのリセット処理
		[RPC]
		public void GameResetRPC(){
				Time.timeScale = 1.0f;
				//退出処理
				PhotonNetwork.LeaveRoom ();
				PhotonNetwork.Disconnect ();
				Application.LoadLevel ("Game");
		}
		private void DelayGameReset(){
				m_PhotonView.RPC ("GameResetRPC", PhotonTargets.All);
				if(PhotonNetwork.offlineMode){//disconnectしたプレイヤーはRPCを撃つことができないので
					GameResetRPC ();
				}
		}
		public void GameReset(){
				Debug.Log ("GameReset!!!!!!");

				Time.timeScale = 1.0f;
				Invoke ("DelayGameReset", 2.0f);
		}

		/// <summary>
		/// レジューム復帰した際の処理について
		/// </summary>
		/// <param name="pauseStatus">If set to <c>true</c> pause status.</param>
		void OnApplicationPause (bool pauseStatus)
		{
		if (PhotonNetwork.player == null)
			return;
		
				if (!PhotonNetwork.player.isMasterClient)
						return;

				if (pauseStatus) {//バックグラウンドへの移行時
						Debug.Log("applicationWillResignActive or onPause");
						//GameManager.Instance.GameStop ();
						GameReset ();
				} else {//バックグラウンドからのアプリ復帰時
						Debug.Log("applicationDidBecomeActive or onResume");
						//GameManager.Instance.GameResume ();
						GameReset ();
				}

		}

		public void addNetworkPlayer(bool _isComputer=false){
				Debug.Log ("addNetworkPlayer");
				//PlayerPonZyzra内部でPhotonPlayerListにaddする処理をしている
				GameObject _obj = PhotonNetwork.Instantiate (objectName, Vector3.zero, Quaternion.identity, 0);

				Player _Player = _obj.GetComponent<Player> ();
				_Player.isPlayer = true;










				//---------
				//_Player.AssignedCharaList = CharaSelectUIManager.Instance.CharaSelectAssignedCharaList;
				//_Player.LeaderNumber = 0;
				//_Player.AssignedCharaList = GameManager.Instance.old_Players [PlayerNumber].AssignedCharaList;
				//_Player.LeaderNumber = GameManager.Instance.old_Players [PlayerNumber].LeaderNumber;

		}
		#region IPunCallbacks

		//photonに繋いだとき
		public void OnConnectedToPhoton() {
				Debug.Log ("OnConnectedToPhoton");
		}
		//部屋を退出したとき
		public void OnLeftRoom() {
				GameReset ();
				Debug.Log ("OnLeftRoom");
		}
		//MasterClientが変わったとき
		public void OnMasterClientSwitched(PhotonPlayer newMasterClient) {
				Debug.Log ("OnMasterClientSwitched");
		}
		//部屋の作成に失敗した時
		public void OnPhotonCreateRoomFailed(object[] codeAndMsg) {
				Debug.Log ("OnPhotonCreateRoomFailed");
		}
		//部屋への入室に失敗したとき
		public void OnPhotonJoinRoomFailed(object[] codeAndMsg) {
				Debug.Log ("OnPhotonJoinRoomFailed");
				GameResetRPC ();
		}

		//ロビーから退出したとき
		public void OnLeftLobby() {
				Debug.Log ("OnLeftLobby");
		}
		//photoへの接続に失敗したとき
		public void OnFailedToConnectToPhoton(DisconnectCause cause) {
				GameReset ();
				Debug.Log ("OnFailedToConnectToPhoton");
		}
		//接続に失敗したとき
		public void OnConnectionFail(DisconnectCause cause) {
				GameReset ();
				Debug.Log ("OnConnectionFail");
		}
		//切断したとき
		public void OnDisconnectedFromPhoton() {
				Debug.Log ("OnDisconnectedFromPhoton");
				GameReset ();
		}
		//PhotonがInstantitaされたとき
		public void OnPhotonInstantiate(PhotonMessageInfo info) {
				Debug.Log ("OnPhotonInstantiate");
		}
		//RoomListへのUPDateが走ったとき
		public void OnReceivedRoomListUpdate() {
				Debug.Log ("OnReceivedRoomListUpdate");
		}
		//PhotonPlayerが接続したとき
		public void OnPhotonPlayerConnected(PhotonPlayer newPlayer) {
				Debug.Log ("OnPhotonPlayerConnected");
		}
		//プレイヤーが誰かしら切断した時
		public void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer) {
				GameReset ();
				Debug.Log ("OnPhotonPlayerDisconnected");
		}
		//Masterへ接続したとき
		public void OnConnectedToMaster() {
				Debug.Log ("OnConnectedToMaster");
		}
		//?
		public void OnPhotonMaxCccuReached() {
				Debug.Log ("OnPhotonMaxCccuReached");
		}
		//CustomRoomProperが変わったとき
		public void OnPhotonCustomRoomPropertiesChanged(Hashtable propertiesThatChanged) {
				Debug.Log ("OnPhotonCustomRoomPropertiesChanged");
		}
		//PhotonPlayerのProperが変わったとき
		public void OnPhotonPlayerPropertiesChanged(object[] playerAndUpdatedProps) {
				Debug.Log ("OnPhotonPlayerPropertiesChanged");
		}
		//FriendListが変わったとき
		public void OnUpdatedFriendList() {
				Debug.Log ("OnUpdatedFriendList");
		}
		//?
		public void OnCustomAuthenticationFailed(string debugMessage) {
				Debug.Log ("OnCustomAuthenticationFailed");
		}
		//WebRpcからResponseを受け取ったとき
		public void OnWebRpcResponse(OperationResponse response) {
				Debug.Log ("OnWebRpcResponse");
		}
		//OwnershipRequestが来たとき
		public void OnOwnershipRequest(object[] viewAndPlayer) {
				Debug.Log ("OnOwnershipRequest");
		}

		#endregion
	
}
