﻿using UnityEngine;
using System.Collections;
using UniRx;

public class PhotonRoomListModel : MonoBehaviour {

		//ReactivePropertyの作成と初期化
		private ReactiveProperty<RoomInfo[]> _reactiveRooms = new ReactiveProperty<RoomInfo[]> (new RoomInfo[0]);

		public IObservable<RoomInfo[]> CurrentRoomsObservable{
				get{ return _reactiveRooms.AsObservable (); }
		}

		private void OnReceiveRoomListUpdate(){

				_reactiveRooms.Value = PhotonNetwork.GetRoomList ();

		}
}
