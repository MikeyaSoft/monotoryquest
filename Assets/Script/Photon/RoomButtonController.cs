﻿using UnityEngine;
using System.Collections;

public class RoomButtonController : MonoBehaviour {

		public UIButton _UIButton;
		public UILabel _UILabel;
		public void JoinRoom(){
				PhotonNetwork.JoinRoom(UIButton.current.name);
				LobbySceneController.Instance.gameObject.SetActive (false);
				PhotonNetwork.player.name = LobbySceneController.Instance.GetInPutLabelText();
		}
}
